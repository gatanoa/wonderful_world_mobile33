import 'app_command.dart';
import '../warpreflection_center.dart';
import '../isolate/command/isolate_command.dart';

class JiffmillisecdurationCommand implements AppCommand {
  final int millisec;

  const JiffmillisecdurationCommand(this.millisec);

  @override
  Future<IsolateCommand> exec(WarpReflectionCenter receiver) async {
    return IsolateCommand.empty;
  }
}
