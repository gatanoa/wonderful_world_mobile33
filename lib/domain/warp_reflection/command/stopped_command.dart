import 'app_command.dart';
import '../warpreflection_center.dart';
import '../isolate/command/isolate_command.dart';

class StoppedCommand implements AppCommand {
  @override
  Future<IsolateCommand> exec(WarpReflectionCenter receiver) async {
    receiver.stoppedObservable.add(null);
    return IsolateCommand.empty;
  }
}