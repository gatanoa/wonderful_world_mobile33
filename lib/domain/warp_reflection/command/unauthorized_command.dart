import 'app_command.dart';
import '../warpreflection_center.dart';
import '../isolate/command/isolate_command.dart';

class UnauthorizedCommand implements AppCommand {
  final IsolateCommand parentCommand;

  const UnauthorizedCommand(this.parentCommand);

  @override
  Future<IsolateCommand> exec(WarpReflectionCenter receiver) async {
    var token = await receiver.newToken();
    if (token.isNotEmpty) {
      return IsolateCommand.setToken(token, parentCommand);
    } else {
      return IsolateCommand.empty;
    }
  }
}
