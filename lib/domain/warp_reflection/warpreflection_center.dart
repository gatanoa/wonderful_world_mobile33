import 'dart:async';
import 'dart:isolate';
import 'dart:math';

import 'package:path_provider/path_provider.dart';
import 'package:koin_flutter/koin_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/repository/network/user_repository/user_repository.dart';
import 'package:wonderful_world/domain/user_center.dart';
import 'package:wonderful_world/domain/warp_reflection/command/jiffmillisecduration_command.dart';

import 'command/app_command.dart';
import 'command/loadingcompleted_command.dart';
import 'command/loadingfailed_command.dart';
import 'command/savecompleted_command.dart';
import 'command/savefailed_command.dart';
import 'isolate/command/isolate_command.dart';
import 'isolate/isolate_main.dart';
import 'model/reflect_item.dart';

class WarpReflectionCenter implements Disposable {
  final UserRepository _userRepository;
  final UserCenter _userCenter;

  StreamSubscription _exitSubscription;
  StreamSubscription _commandSubscription;
  StreamSubscription _authStateSubscription;

  PublishSubject<List<ReflectItem>> _reflectionObservable =
      PublishSubject<List<ReflectItem>>();
  PublishSubject<List<ReflectItem>> get reflectionObservable =>
      _reflectionObservable;
  PublishSubject<void> _stoppedObservable = PublishSubject<void>();
  PublishSubject<void> get stoppedObservable => _stoppedObservable;
  PublishSubject<void> _changedObservable = PublishSubject<void>();
  PublishSubject<void> get changedObservable => _changedObservable;

  ReceivePort _receiveExitPort;
  ReceivePort _receivePort;
  SendPort _sendPort;

  Isolate _isolate;
  Stream _receiveStream;

  WarpReflectionCenter(this._userCenter, this._userRepository) {
    _receivePort = ReceivePort();
    _receiveStream = _receivePort.asBroadcastStream();

    _receiveExitPort = ReceivePort();

    _authStateSubscription = _userCenter.authStateChangeObservable.listen(
      _authStateChange,
    );
    _exitSubscription = _receiveExitPort.asBroadcastStream().listen(
          (_) => _exitListener(),
        );
    _commandSubscription = _receiveStream
        .where(
          (event) => event is AppCommand,
        )
        .listen(
          (command) => _commandListener(command),
        );
  }

  Future<void> startIfStopped() async {
    if (_isolate == null) {
      var receiveStream = _receiveStream;
      _isolate = await Isolate.spawn(
        isolateMain,
        _receivePort.sendPort,
        onExit: _receiveExitPort.sendPort,
      );
      _sendPort = await receiveStream.first;
      var appDir = await getApplicationDocumentsDirectory();
      _sendPort.send(appDir.path);
      _sendPort = await receiveStream.first;
      bool bufferIsAuth;
      try {
        bufferIsAuth = await _userCenter.isAuth();
      } catch (_) {
        bufferIsAuth = false;
      }
      _authStateChange(bufferIsAuth);
    }
  }

  Future<int> jiffMillisecDuration() async {
    _sendPort.send(IsolateCommand.getJiffMilliSecDuration());
    var result = await _receiveStream.where((command) {
      return command is JiffmillisecdurationCommand;
    }).first;
    return (result as JiffmillisecdurationCommand).millisec;
  }

  Future<bool> loading(String id) async {
    _sendPort.send(IsolateCommand.loading(id));
    var result = await _receiveStream.where((command) {
      return command is LoadingCompletedCommand ||
          command is LoadingFailedCommand;
    }).first;
    return result is LoadingCompletedCommand;
  }

  Future<bool> lastDataLoading() async {
    _sendPort.send(IsolateCommand.lastDataLoading());
    var result = await _receiveStream.where((command) {
      return command is LoadingCompletedCommand ||
          command is LoadingFailedCommand;
    }).first;
    return result is LoadingCompletedCommand;
  }

  Future<bool> save() async {
    _sendPort.send(IsolateCommand.save());
    var result = await _receiveStream.where((command) {
      return command is SaveCompletedCommand || command is SaveFailedCommand;
    }).first;
    return result is SaveCompletedCommand;
  }

  void insert(Point<int> point) {
    _sendPort.send(IsolateCommand.insert(point));
    _changedObservable.add(null);
  }

  void remove(Point<int> point) {
    _sendPort.send(IsolateCommand.remove(point));
    _changedObservable.add(null);
  }

  void getReflection() {
    _sendPort.send(IsolateCommand.getReflection());
  }

  void setJiffMilliSecDuration(int milliseconds) {
    _sendPort.send(IsolateCommand.setJiffMilliSecDuration(milliseconds));
  }

  void start() {
    _sendPort.send(IsolateCommand.start());
  }

  void stop() {
    _sendPort.send(IsolateCommand.stop());
  }

  void clear() {
    _sendPort.send(IsolateCommand.clear());
  }

  void _authStateChange(bool isAuth) {
    if (isAuth) {
      _userRepository.token().then(
        (token) {
          _sendPort.send(IsolateCommand.setToken(token, IsolateCommand.empty));
        },
      );
    }
  }

  void _exitListener() {
    _isolate = null;
  }

  void _commandListener(AppCommand command) {
    command.exec(this).then((result) {
      if (result.isNotEmpty) {
        _sendPort.send(result);
      }
    });
  }

  Future<String> newToken() async {
    if (await _userCenter.isAuth(isUpgrade: true)) {
      return await _userRepository.token();
    } else {
      return '';
    }
  }

  Future<void> closeIsolate({bool isKill = false}) async {
    if (_isolate != null) {
      if (isKill) {
        _isolate.kill(priority: Isolate.immediate);
      } else {
        _sendPort.send(IsolateCommand.exit());
      }
    }
    _sendPort = null;
  }

  @override
  void dispose() {
    _stoppedObservable.close();
    _reflectionObservable.close();
    _authStateSubscription.cancel();
    _commandSubscription.cancel();
    _exitSubscription.cancel();
    _receiveExitPort.close();
    _receivePort.close();
    closeIsolate(isKill: true);
  }
}
