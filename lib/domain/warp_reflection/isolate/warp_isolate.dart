import 'dart:async';
import 'dart:isolate';

import 'package:koin/koin.dart';
import 'package:koin_flutter/koin_disposable.dart';
import 'package:wonderful_world/domain/warp_reflection/command/app_command.dart';
import 'package:wonderful_world/domain/warp_reflection/model/reflect_item.dart';

import 'command/exit_command.dart';
import 'command/isolate_command.dart';
import 'controller/warp_controller.dart';

class WarpIsolate extends KoinComponent implements Disposable {
  final SendPort _sendPort;
  final ReceivePort _receivePort;
  WarpController _warpController;
  StreamSubscription<List<ReflectItem>> _sendReflectionSubscription;
  StreamSubscription _stoppedSubscription;

  WarpIsolate(this._sendPort, this._receivePort) {
    _warpController = get();
    _sendReflectionSubscription =
        _warpController.reflectionShotObservable.listen(
      (items) => _sendReflectionCallback(items),
    );
    _stoppedSubscription = _warpController.stoppedObservable.listen(
      (_) => _stoppedListener(),
    );
  }

  void _send(AppCommand command) {
    _sendPort.send(command);
  }

  void _sendReflectionCallback(List<ReflectItem> items) {
    _send(AppCommand.reflectionShot(items));
  }

  void _stoppedListener() {
    _send(AppCommand.stopped());
  }

  Future<void> commandLoop() async {
    await for (IsolateCommand command in _receivePort) {
      if (command is ExitCommand) {
        return;
      } else {
        var result = await command.exec(_warpController);
        if (result.isNotEmpty) {
          _send(result);
        }
      }
    }
  }

  @override
  Future<void> dispose() async {
    await _sendReflectionSubscription.cancel();
    await _stoppedSubscription.cancel();
  }
}
