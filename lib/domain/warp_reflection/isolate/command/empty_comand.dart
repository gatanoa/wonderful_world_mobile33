import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class EmptyCommand implements IsolateCommand {
  const EmptyCommand();
  @override
  Future<AppCommand> exec(WarpController receiver) async {
    return AppCommand.empty;
  }
}
