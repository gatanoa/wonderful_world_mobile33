import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class LastDataLoadingCommand implements IsolateCommand {
  @override
  Future<AppCommand> exec(WarpController receiver) async {
    try {
      await receiver.lastDataLoading();
      return AppCommand.loadingCompleted();
    } catch (ex) {
      return AppCommand.loadingFailed();
    }
  }
}
