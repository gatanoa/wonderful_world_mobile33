import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class StopCommand implements IsolateCommand {
  @override
  Future<AppCommand> exec(WarpController receiver) async {
    receiver.stop();
    return AppCommand.empty;
  }
}
