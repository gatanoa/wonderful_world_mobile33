import 'dart:math';

import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class RemoveCommand implements IsolateCommand {
  final Point<int> point;

  const RemoveCommand(this.point);

  @override
  Future<AppCommand> exec(WarpController receiver) async {
    receiver.remove(point);
    return AppCommand.empty;
  }
}
