import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class SetJiffMilliSecDurationCommand implements IsolateCommand {
  final int milliseconds;

  const SetJiffMilliSecDurationCommand(this.milliseconds);

  @override
  Future<AppCommand> exec(WarpController receiver) async {
    receiver.setJiffTimer(Duration(milliseconds: milliseconds));
    return AppCommand.empty;
  }
}
