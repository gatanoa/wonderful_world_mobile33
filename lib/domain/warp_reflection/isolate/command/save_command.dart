import 'dart:io';

import 'package:dio/dio.dart';

import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class SaveCommand implements IsolateCommand {
  @override
  Future<AppCommand> exec(WarpController receiver) async {
    try {
      await receiver.save();
      return AppCommand.saveCompleted();
    } catch (ex) {
      if (ex is DioError) {
        DioError dioEx = ex;
        if (dioEx.response.statusCode == HttpStatus.unauthorized) {
          return AppCommand.unauthorized(this);
        }
      }
      return AppCommand.saveFailed();
    }
  }
}
