import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class StartCommand implements IsolateCommand {
  @override
  Future<AppCommand> exec(WarpController receiver) async {
    receiver.start();
    return AppCommand.empty;
  }
}
