import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class SetTokenCommand implements IsolateCommand {
  final String token;
  final IsolateCommand parentCommand;

  const SetTokenCommand(this.token, this.parentCommand);

  @override
  Future<AppCommand> exec(WarpController receiver) async {
    receiver.setToken(token);
    return parentCommand.exec(receiver);
  }
}
