import 'package:koin/koin.dart';
import 'package:koin_flutter/koin_disposable.dart';
import 'package:wonderful_world/domain/warp_reflection/isolate/controller/warp_controller.dart';

var warpControllerModule = Module()
  ..disposable((scope) => WarpController(scope.get(), scope.get()));
