import 'dart:async';
import 'dart:math';

import 'package:koin_flutter/koin_disposable.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/repository/local/db_repository/db_repository.dart';
import 'package:wonderful_world/data/repository/network/world_data_repository/world_data_repository.dart';
import 'package:wonderful_world/domain/warp_reflection/isolate/controller/runner/warp_runner.dart';
import 'package:wonderful_world/domain/warp_reflection/model/reflect_item.dart';

class WarpController implements Disposable {
  static const kJiffDuration = Duration(milliseconds: 100);

  final WorldDataRepository _repository;
  final DbRepository _dbRepository;
  final PublishSubject<void> _stoppedObservable = PublishSubject<void>();
  PublishSubject<void> get stoppedObservable => _stoppedObservable;
  final PublishSubject<List<ReflectItem>> _reflectionShotObservable =
      PublishSubject<List<ReflectItem>>();
  PublishSubject get reflectionShotObservable => _reflectionShotObservable;

  WarpRunner _runner;
  Duration _jiffDuration = kJiffDuration;
  Duration get jiffDuration => _jiffDuration;
  Timer _timer;
  String _id;
  bool get isWorldLoaded => _id != null;

  WarpController(this._repository, this._dbRepository);

  void setToken(String token) {
    _repository?.setToken(token);
  }

  Future<void> loading(String id) async {
    var data = await _repository.get(id);
    await _dbRepository.saveLastWorldData(data.data);
    clear();
    _id = id;
    _runner = WarpRunner.fromData(data);
  }

  Future<void> lastDataLoading() async {
    var data = await _dbRepository.loadLastWorldData();
    clear();
    _runner = WarpRunner.fromData(data);
  }

  Future<bool> save() async {
    if (isWorldLoaded) {
      var data = _runner?.getData();
      await _dbRepository.saveLastWorldData(data);
      if (_id != null) {
        return await _repository.save(_id, data);
      } else {
        return true;
      }
    }
    return false;
  }

  void insert(Point<int> point) {
    _runner?.insert(point);
  }

  void remove(Point<int> point) {
    _runner?.remove(point);
  }

  List<ReflectItem> getReflection() {
    return _runner?.getReflection();
  }

  void setJiffTimer(Duration duration) {
    _jiffDuration = duration;
  }

  void start() {
    _tick();
  }

  void _tick() {
    if (_runner == null) {
      _timer = null;
      return;
    }

    var beginTime = DateTime.now();
    if (_runner.jiff()) {
      _reflectionShotObservable.add(_runner.getReflection());

      var difference = DateTime.now().difference(beginTime).abs();
      var duration;
      if (_jiffDuration <= difference) {
        duration = Duration.zero;
      } else {
        duration = _jiffDuration - difference;
      }

      _timer = Timer(duration, _tick);
    } else {
      _timer = null;
      _stoppedObservable.add(null);
    }
  }

  void stop() {
    _timer?.cancel();
    _timer = null;
  }

  void clear() {
    stop();
    _runner = null;
    _id = null;
  }

  @override
  void dispose() {
    clear();
    _reflectionShotObservable.close();
    _stoppedObservable.close();
  }
}
