import 'package:koin/koin.dart';
import 'package:koin_flutter/koin_disposable.dart';
import 'package:wonderful_world/domain/settings_center.dart';
import 'package:wonderful_world/domain/user_center.dart';
import 'package:wonderful_world/domain/warp_reflection/warpreflection_center.dart';
import 'package:wonderful_world/domain/world_center.dart';

var domainModule = Module()
  ..disposable((scope) => SettingsCenter(scope.get()))
  ..disposable((scope) => UserCenter(scope.get()))
  ..disposable((scope) => WorldCenter(scope.get(), scope.get()))
  ..disposable((scope) => WarpReflectionCenter(scope.get(), scope.get()));