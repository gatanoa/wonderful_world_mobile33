import 'package:flutter/material.dart';

final Color fDefaultReflectionBackgroundColor = Colors.white;
final Color fDefaultReflectionCell0Color = Colors.purple[700];
final Color fDefaultReflectionGreedColor = Colors.grey;
