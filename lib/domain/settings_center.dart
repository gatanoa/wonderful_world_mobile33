import 'package:flutter/material.dart';
import 'package:koin_flutter/koin_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/repository/local/settings_repository/settings_repository.dart';
import 'package:wonderful_world/domain/constants/colors.dart';

class SettingsCenter implements Disposable {
  final SettingsRepository _settingsRepository;
  final PublishSubject<bool> _darkModeStateObservable = PublishSubject<bool>();
  PublishSubject<bool> get darkModeStateObservable => _darkModeStateObservable;

  SettingsCenter(this._settingsRepository);

  Future<bool> getDarkModeState() async {
    var state = await _settingsRepository.getDarkModeState();
    return state ?? true;
  }

  Future<void> setDarkModeState(bool state) async {
    await _settingsRepository.setDarkModeState(state);
    _darkModeStateObservable.add(state);
  }

  Future<Color> getReflectionBackgroundColor() async {
    var color = await _settingsRepository.getReflectionBackgroundColor();
    return (color != null) ? Color(color) : fDefaultReflectionBackgroundColor;
  }

  Future<void> setReflectionBackgroundColor(Color color) async {
    return await _settingsRepository.setReflectionBackgroundColor(color.value);
  }

  Future<Color> getReflectionCell0Color() async {
    var color = await _settingsRepository.getReflectionCell0Color();
    return (color != null) ? Color(color) : fDefaultReflectionCell0Color;
  }

  Future<void> setReflectionCell0Color(Color color) async {
    return await _settingsRepository.setReflectionCell0Color(color.value);
  }

  Future<Color> getReflectionGreedColor() async {
    var color = await _settingsRepository.getReflectionGreedColor();
    return (color != null) ? Color(color) : fDefaultReflectionGreedColor;
  }

  Future<void> setReflectionGreedColor(Color color) async {
    return await _settingsRepository.setReflectionGreedColor(color.value);
  }

  Future<void> resetReflectionColors() async {
    await setReflectionBackgroundColor(fDefaultReflectionBackgroundColor);
    await setReflectionCell0Color(fDefaultReflectionCell0Color);
    await setReflectionGreedColor(fDefaultReflectionGreedColor);
  }

  @override
  void dispose() {
    _darkModeStateObservable.close();
  }
}
