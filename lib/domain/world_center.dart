import 'package:dio/dio.dart';
import 'package:koin_flutter/koin_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_add_req_body/world_add_req_body.dart';
import 'package:wonderful_world/data/repository/local/db_repository/db_repository.dart';
import 'package:wonderful_world/data/repository/network/world_repository/world_repository.dart';

class WorldCenter implements Disposable {
  final DbRepository _dbRepository;
  final WorldRepository _worldRepository;

  List<String> _selectedIds = List<String>.empty();

  final PublishSubject<void> _worldsChangeObservable = PublishSubject<void>();
  PublishSubject<void> get worldsChangeObservable => _worldsChangeObservable;

  WorldCenter(this._dbRepository, this._worldRepository);

  Future<bool> create(
    String name,
    int type,
    String description,
    int width,
    int height,
  ) async {
    try {
      await _worldRepository.add(
        WorldAddReqBody(name, description, width, height, type),
      );
      _worldsChangeObservable.add(null);
      return true;
    } catch (_) {
      return false;
    }
  }

  Future<List<World>> _idsToWorlds(List<String> ids) async {
    return await Stream.fromIterable(ids).asyncMap(
      (id) async {
        return await _worldRepository.world(id);
      },
    ).toList();
  }

  Future<List<World>> search(
      String name, String authorId, String authorName) async {
    return await _idsToWorlds(
      await _worldRepository.search(
        name: name,
        authorId: authorId,
        authorName: authorName,
      ),
    );
  }

  Future<List<World>> worlds() async {
    return await _idsToWorlds(await _worldRepository.worlds());
  }

  Future<List<World>> createdWorlds() async {
    return await _idsToWorlds(await _worldRepository.createdWorlds());
  }

  Future<List<World>> selectedWorlds() async {
    _selectedIds = await _worldRepository.selectedWorlds();
    return await _idsToWorlds(_selectedIds);
  }

  Future<bool> selectedWorldsContains(String id) async {
    if (_selectedIds.isEmpty) {
      _selectedIds = await _worldRepository.selectedWorlds();
    }
    return _selectedIds.contains(id);
  }

  Future<bool> selectedWorldAdd(String id) async {
    try {
      var result = await _worldRepository.selectedWorldAdd(id);
      _worldsChangeObservable.add(null);
      return result;
    } on DioError catch (_) {
      return false;
    }
  }

  Future<bool> selectedWorldRemove(String id) async {
    try {
      var result = await _worldRepository.selectedWorldRemove(id);
      _worldsChangeObservable.add(null);
      return result;
    } on DioError catch (_) {
      return false;
    }
  }

  Future<bool> clone(String id) async {
    try {
      await _worldRepository.clone(id);
      _worldsChangeObservable.add(null);
      return true;
    } on DioError catch (_) {
      return false;
    }
  }

  Future<bool> remove(String id) async {
    try {
      var result = await _worldRepository.remove(id);
      _worldsChangeObservable.add(null);
      return result;
    } on DioError catch (_) {
      return false;
    }
  }

  Future<void> setLastWorld(String worldId) async {
    var bufferWorld = await _worldRepository.world(worldId);
    await _dbRepository.saveLastWorld(bufferWorld);
  }

  Future<World> getLastWorld() async {
    return await _dbRepository.loadLastWorld();
  }

  @override
  void dispose() {
    _worldsChangeObservable.close();
  }
}
