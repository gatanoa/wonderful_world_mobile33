import 'package:flutter/material.dart';
import 'package:koin/koin.dart';
import 'package:wonderful_world/data/di_module.dart';
import 'package:wonderful_world/domain/di_module.dart';
import 'package:wonderful_world/ui/di_module.dart';

import 'ui/app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  startKoin((app) {
    app.printLogger(level: Level.error);
    app.modules(<Module>[
      ...dataModules,
      domainModule,
      ...uiModules,
    ]);
  });

  runApp(App());
}
