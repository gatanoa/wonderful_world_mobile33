import 'package:wonderful_world/data/data_source/di_modules.dart';
import 'package:wonderful_world/data/data_source/network/world_data/di_module.dart';
import 'package:wonderful_world/data/repository/di_module.dart';
import 'package:wonderful_world/data/repository/network/world_data_repository/di_module.dart';

import 'repository/local/db_repository/di_module.dart';

var dataModules = [
  ...dataSourceModules,
  ...repositoryModules,
];

var worldDataDataModules = [
  dbRpositoryModule,
  worldDataNetworkModule,
  worldDataReposytoryModule,
];
