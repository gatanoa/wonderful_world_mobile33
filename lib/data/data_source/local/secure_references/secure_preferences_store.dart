abstract class SecurePreferencesStore {
  Future<String> getString(String key);
  Future<bool> setString(String key, String value);
  Future<bool> clear();
  Future<void> reload();
}
