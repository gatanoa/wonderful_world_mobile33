import 'package:koin/koin.dart';

import 'secure_preferences_store.dart';
import 'secure_preferences_store_impl.dart';

var securePreferencesStoreModule = Module()
  ..single<SecurePreferencesStore>(
    (scope) => SecurePreferencesStoreImpl(),
  );
