import 'package:koin/koin.dart';

import 'preferences_store.dart';
import 'preferences_store_impl.dart';

/// var pref =  await SharedPreferences.getInstance();
/// Need ..single((scope) => pref);
var preferencesStoreModule = Module()
  ..single<PreferencesStore>((scope) => PreferencesStoreImpl());
