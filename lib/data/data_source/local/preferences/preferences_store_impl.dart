import 'package:shared_preferences/shared_preferences.dart';
import 'package:wonderful_world/data/data_source/local/preferences/preferences_store.dart';

class PreferencesStoreImpl implements PreferencesStore {
  SharedPreferences _preferences;

  Future<void> _initPrefs() async {
    _preferences ??= await SharedPreferences.getInstance();
  }

  @override
  Future<bool> clear() async {
    await _initPrefs();
    return await _preferences.clear();
  }

  @override
  Future<bool> containsKey(String key) async {
    await _initPrefs();
    return _preferences.containsKey(key);
  }

  @override
  Future<dynamic> get(String key) async {
    await _initPrefs();
    return _preferences.get(key);
  }

  @override
  Future<bool> getBool(String key) async {
    await _initPrefs();
    return _preferences.getBool(key);
  }

  @override
  Future<double> getDouble(String key) async {
    await _initPrefs();
    return _preferences.getDouble(key);
  }

  @override
  Future<int> getInt(String key) async {
    await _initPrefs();
    return _preferences.getInt(key);
  }

  @override
  Future<String> getString(String key) async {
    await _initPrefs();
    return _preferences.getString(key);
  }

  @override
  Future<List<String>> getStringList(String key) async {
    await _initPrefs();
    return _preferences.getStringList(key);
  }

  @override
  Future<void> reload() async {
    await _initPrefs();
    return await _preferences.reload();
  }

  @override
  Future<bool> remove(String key) async {
    await _initPrefs();
    return await _preferences.remove(key);
  }

  @override
  Future<bool> setBool(String key, bool value) async {
    await _initPrefs();
    return await _preferences.setBool(key, value);
  }

  @override
  Future<bool> setDouble(String key, double value) async {
    await _initPrefs();
    return await _preferences.setDouble(key, value);
  }

  @override
  Future<bool> setInt(String key, int value) async {
    await _initPrefs();
    return await _preferences.setInt(key, value);
  }

  @override
  Future<bool> setString(String key, String value) async {
    await _initPrefs();
    return await _preferences.setString(key, value);
  }

  @override
  Future<bool> setStringList(String key, List<String> value) async {
    await _initPrefs();
    return await _preferences.setStringList(key, value);
  }
}
