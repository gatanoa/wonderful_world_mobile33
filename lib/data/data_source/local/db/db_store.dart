import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_data/world_data.dart';

abstract class DbStore {
  Future<WorldComponents> loadWorldDataComponents(int key);
  Future<void> saveWorldDataComponents(int key, WorldComponents components);
  Future<void> removeWorldDataComponents(int key);
  Future<World> loadWorld(int key);
  Future<void> saveWorld(int key, World world);
  Future<void> removeWorld(int key);
}
