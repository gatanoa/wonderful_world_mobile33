import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_data/world_data.dart';

import 'db_store.dart';

class DbStoreImpl extends DbStore {
  static const _kWorldKey = 'world';
  Box<World> _boxWorld;
  static const _kWorldDataCell0Key = 'worldDataCell0_';
  bool _isNotInit = true;
  final String path;

  DbStoreImpl({this.path = ''});

  Future<void> _init() async {
    if (_isNotInit) {
      _isNotInit = false;
      if (path.isEmpty) {
        await Hive.initFlutter();
      } else {
        Hive.init(path);
      }
      Hive.registerAdapter(WorldAdapter());
      Hive.registerAdapter(CellT0Adapter());
    }
  }

  Future<void> _initWorldBox() async {
    if (_boxWorld == null) {
      await _init();
      _boxWorld = await Hive.openBox<World>(_kWorldKey);
    }
  }

  Future<Box<CellT0>> _getWorldDataCell0Box(int key) async {
    await _init();
    return await Hive.openBox<CellT0>(
      _kWorldDataCell0Key + key.toString(),
    );
  }

  @override
  Future<World> loadWorld(int key) async {
    await _initWorldBox();
    return _boxWorld.get(
      key,
      defaultValue: World.empty,
    );
  }

  @override
  Future<void> saveWorld(int key, World world) async {
    await _initWorldBox();
    await _boxWorld.put(key, world);
  }

  @override
  Future<void> removeWorld(int key) async {
    await _initWorldBox();
    await _boxWorld.delete(key);
  }

  @override
  Future<WorldComponents> loadWorldDataComponents(int key) async {
    var bufferBox = await _getWorldDataCell0Box(key);
    return WorldComponents(bufferBox.values.toList());
  }

  @override
  Future<void> saveWorldDataComponents(
    int key,
    WorldComponents components,
  ) async {
    var bufferBox = await _getWorldDataCell0Box(key);
    if (bufferBox.length > 0) {
      await bufferBox.clear();
    }
    await bufferBox.addAll(components.cells0);
  }

  @override
  Future<void> removeWorldDataComponents(int key) async {
    var bufferBox = await _getWorldDataCell0Box(key);
    await bufferBox.deleteFromDisk();
  }
}
