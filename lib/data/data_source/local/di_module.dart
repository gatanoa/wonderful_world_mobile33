import 'package:koin/koin.dart';
import 'package:wonderful_world/data/data_source/local/db/di_module.dart';
import 'package:wonderful_world/data/data_source/local/token_store.dart';

import 'preferences/di_module.dart';
import 'secure_references/di_module.dart';

var localDataModules = [
  dbStoreModule,
  preferencesStoreModule,
  securePreferencesStoreModule,
  Module()..single((scope) => TokenStore(scope.get())),
];
