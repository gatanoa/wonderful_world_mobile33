import 'local/di_module.dart';
import 'network/di_module.dart';

var dataSourceModules = [
  ...localDataModules,
  networkModule,
];
