import 'package:dio/dio.dart';
import 'package:koin/koin.dart';

import 'world_data_network_api.dart';

var worldDataNetworkModule = Module()
  ..single((scope) => Dio())
  ..single((scope) => WorldDataNetworkApi(scope.get()));
