// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'world_data_network_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _WorldDataNetworkApi implements WorldDataNetworkApi {
  _WorldDataNetworkApi(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://cwelt.net/';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<WorldData> getWorldData(id) async {
    ArgumentError.checkNotNull(id, 'id');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('wfw/api/data/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = WorldData.fromJson(_result.data);
    return value;
  }

  @override
  Future<void> saveWorldData(authorization, id, worldData) async {
    ArgumentError.checkNotNull(authorization, 'authorization');
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(worldData, 'worldData');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(worldData?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    await _dio.request<void>('wfw/api/data/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{r'authorization': authorization},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    return null;
  }
}
