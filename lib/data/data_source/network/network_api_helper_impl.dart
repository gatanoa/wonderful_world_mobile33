import 'dart:io';

import 'package:dio/dio.dart';
import 'package:wonderful_world/data/data_source/network/token_provider.dart';
import 'package:wonderful_world/data/data_source/network/network_api.dart';
import 'package:wonderful_world/data/data_source/network/network_api_helper.dart';
import 'package:wonderful_world/data/model/token/token.dart';
import 'package:wonderful_world/data/model/world_add_req_body/world_add_req_body.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/data/model/register_body/register_body.dart';
import 'package:wonderful_world/data/model/login/login_body.dart';

class NetworkApiHelperImpl implements NetworkApiHelper {
  final NetworkApi _networkApi;
  final TokenProvider _tokenProvider;

  const NetworkApiHelperImpl(this._networkApi, this._tokenProvider);

  @override
  Future<bool> check() async {
    try {
      await _networkApi.check(
        await _tokenProvider.get(),
      );
      return true;
    } on DioError catch (ex) {
      if (ex.response?.statusCode == HttpStatus.unauthorized) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  Future<Token> loginVk(String code) async {
    return await _networkApi.loginVk(code);
  }

  @override
  Future<Token> login(LoginBody loginBody) async {
    return await _networkApi.login(loginBody);
  }

  @override
  Future<Token> register(RegisterBody registerBody) async {
    return await _networkApi.register(registerBody);
  }

  @override
  Future<Token> tokenUpdate() async {
    return await _networkApi.tokenUpdate(
      Token(
        refresh: await _tokenProvider.getRefresh(),
      ),
    );
  }

  @override
  Future<bool> logout() async {
    try {
      await _networkApi.logout(
        await _tokenProvider.get(),
      );
      return true;
    } on DioError catch (_) {
      return false;
    }
  }

  @override
  Future<User> user(String id) async {
    return await _networkApi.user(id);
  }

  @override
  Future<User> current() async {
    return await _networkApi.current(
      await _tokenProvider.get(),
    );
  }

  @override
  Future<bool> existsUsername(String username) async {
    try {
      await _networkApi.existsUsername(username);
      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  Future<bool> existsEmail(String email) async {
    try {
      await _networkApi.existsEmail(email);
      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  Future<World> world(String id) async {
    return await _networkApi.world(id);
  }

  @override
  Future<List<String>> worlds() async {
    return await _networkApi.worlds();
  }

  @override
  Future<List<String>> selectedWorlds() async {
    return await _networkApi.selectedWorlds(
      await _tokenProvider.get(),
    );
  }

  @override
  Future<List<String>> createdWorlds() async {
    return await _networkApi.createdWorlds(
      await _tokenProvider.get(),
    );
  }

  @override
  Future<List<String>> search({
    String name,
    String authorId,
    String authorName,
  }) async {
    return await _networkApi.search(
      name: name,
      authorId: authorId,
      authorName: authorName,
    );
  }

  @override
  Future<bool> add(WorldAddReqBody world) async {
    try {
      await _networkApi.add(
        await _tokenProvider.get(),
        world,
      );

      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  Future<bool> clone(String id) async {
    try {
      await _networkApi.clone(
        await _tokenProvider.get(),
        id,
      );

      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  Future<bool> remove(String id) async {
    try {
      await _networkApi.remove(
        await _tokenProvider.get(),
        id,
      );

      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  Future<bool> selectedWorldAdd(String id) async {
    try {
      await _networkApi.selectedWorldAdd(
        await _tokenProvider.get(),
        id,
      );
      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  Future<bool> selectedWorldRemove(String id) async {
    try {
      await _networkApi.selectedWorldRemove(
        await _tokenProvider.get(),
        id,
      );
      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }
}
