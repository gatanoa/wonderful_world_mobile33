import 'package:wonderful_world/data/data_source/local/token_store.dart';

import 'constants.dart';

class TokenProvider {
  final TokenStore _store;
  const TokenProvider(this._store);

  Future<String> get() async {
    return kTokenPrefix + await _store.get();
  }

  Future<String> getRefresh() async {
    return await _store.getRefresh();
  }

  static String tokenTokHeaderRow(String token) {
    return kTokenPrefix + token;
  }
}
