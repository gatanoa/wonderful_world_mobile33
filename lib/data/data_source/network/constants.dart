const String kBaseUrl = 'https://cwelt.net/';
const String kTokenPrefix = 'Bearer ';

const String kPathApiAuthCheck = 'api/auth/check';
const String kPathApiAuthVk = 'api/auth/vk';
const String kPathApiAuthLogin = 'api/auth/login';
const String kPathApiAuthRegister = 'api/auth/register';
const String kPathApiAuthUpdate = 'api/auth/update';
const String kPathApiAuthLogout = 'api/auth/logout';
const String kPathApiAuthExistsUsername = 'api/auth/exists/username/{username}';
const String kPathApiAuthExistsEmail = 'api/auth/exists/email/{email}';

const String kPathApiUsersAt = 'wfw/api/users/{id}';
const String kPathApiUsersCurrent = 'wfw/api/users';

const String kPathApiWorldsAt = 'wfw/api/worlds/{id}';
const String kPathApiWorldsList = 'wfw/api/worlds';
const String kPathApiWorldsSelectedList = 'wfw/api/worlds/selected';
const String kPathApiWorldsCreatedList = 'wfw/api/worlds/created';
const String kPathApiWorldsSearch = 'wfw/api/worlds/search';
const String kPathApiWorldsAdd = 'wfw/api/worlds';
const String kPathApiWorldsClone = 'wfw/api/worlds/clone/{id}';
const String kPathApiWorldsDelete = 'wfw/api/worlds/{id}';
const String kPathApiWorldsSelectedAdd = 'wfw/api/worlds/selected/{id}';
const String kPathApiWorldsSelectedDelete = 'wfw/api/worlds/selected/{id}';

const String kPathApiDataLoad = 'wfw/api/data/{id}';
const String kPathApiDataSave = 'wfw/api/data/{id}';