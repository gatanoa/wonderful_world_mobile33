import 'dart:io';

import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:wonderful_world/data/model/login/login_body.dart';
import 'package:wonderful_world/data/model/register_body/register_body.dart';
import 'package:wonderful_world/data/model/token/token.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_add_req_body/world_add_req_body.dart';

import 'constants.dart';

part 'network_api.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class NetworkApi {
  factory NetworkApi(Dio dio, {String baseUrl}) = _NetworkApi;

  @GET(kPathApiAuthCheck)
  Future<void> check(
    @Header(HttpHeaders.authorizationHeader) String authorization,
  );

  @POST(kPathApiAuthVk)
  Future<Token> loginVk(@Query('code', encoded: true) String code);

  @POST(kPathApiAuthLogin)
  Future<Token> login(@Body() LoginBody loginBody);

  @POST(kPathApiAuthRegister)
  Future<Token> register(@Body() RegisterBody registerBody);

  @PUT(kPathApiAuthUpdate)
  Future<Token> tokenUpdate(@Body() Token token);

  @DELETE(kPathApiAuthLogout)
  Future<void> logout(
    @Header(HttpHeaders.authorizationHeader) String authorization,
  );

  @GET(kPathApiAuthExistsUsername)
  Future<void> existsUsername(@Path('username') String username);

  @GET(kPathApiAuthExistsEmail)
  Future<void> existsEmail(@Path('email') String email);

  @GET(kPathApiUsersAt)
  Future<User> user(@Path('id') String id);

  @GET(kPathApiUsersCurrent)
  Future<User> current(
    @Header(HttpHeaders.authorizationHeader) String authorization,
  );

  @GET(kPathApiWorldsAt)
  Future<World> world(@Path('id') String id);

  @GET(kPathApiWorldsList)
  Future<List<String>> worlds();

  @GET(kPathApiWorldsSelectedList)
  Future<List<String>> selectedWorlds(
    @Header(HttpHeaders.authorizationHeader) String authorization,
  );

  @GET(kPathApiWorldsCreatedList)
  Future<List<String>> createdWorlds(
    @Header(HttpHeaders.authorizationHeader) String authorization,
  );

  @GET(kPathApiWorldsSearch)
  Future<List<String>> search({
    @Query('name', encoded: true) String name = '*',
    @Query('authorId', encoded: true) String authorId = '*',
    @Query('authorName', encoded: true) String authorName = '*',
  });

  @POST(kPathApiWorldsAdd)
  Future<void> add(
    @Header(HttpHeaders.authorizationHeader) String authorization,
    @Body() WorldAddReqBody world,
  );

  @POST(kPathApiWorldsClone)
  Future<void> clone(
    @Header(HttpHeaders.authorizationHeader) String authorization,
    @Path('id') String id,
  );

  @DELETE(kPathApiWorldsDelete)
  Future<void> remove(
    @Header(HttpHeaders.authorizationHeader) String authorization,
    @Path('id') String id,
  );

  @POST(kPathApiWorldsSelectedAdd)
  Future<void> selectedWorldAdd(
    @Header(HttpHeaders.authorizationHeader) String authorization,
    @Path('id') String id,
  );

  @DELETE(kPathApiWorldsSelectedDelete)
  Future<void> selectedWorldRemove(
    @Header(HttpHeaders.authorizationHeader) String authorization,
    @Path('id') String id,
  );
}
