// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'world_add_req_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorldAddReqBody _$WorldAddReqBodyFromJson(Map<String, dynamic> json) {
  return WorldAddReqBody(
    json['name'] as String,
    json['description'] as String,
    json['width'] as int,
    json['height'] as int,
    json['type'] as int,
  );
}

Map<String, dynamic> _$WorldAddReqBodyToJson(WorldAddReqBody instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'width': instance.width,
      'height': instance.height,
      'type': instance.type,
    };
