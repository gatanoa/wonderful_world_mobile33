// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'world_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CellT0Adapter extends TypeAdapter<CellT0> {
  @override
  final int typeId = 1;

  @override
  CellT0 read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CellT0(
      fields[0] as int,
      fields[1] as int,
    );
  }

  @override
  void write(BinaryWriter writer, CellT0 obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.x)
      ..writeByte(1)
      ..write(obj.y);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CellT0Adapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorldData _$WorldDataFromJson(Map<String, dynamic> json) {
  return WorldData(
    json['data'] == null
        ? null
        : WorldComponents.fromJson(json['data'] as Map<String, dynamic>),
    type: json['type'] as int,
    size: json['size'] == null
        ? null
        : NaturalSize.fromJson(json['size'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$WorldDataToJson(WorldData instance) => <String, dynamic>{
      'data': instance.data,
      'type': instance.type,
      'size': instance.size,
    };

WorldComponents _$WorldComponentsFromJson(Map<String, dynamic> json) {
  return WorldComponents(
    (json['cells0'] as List)
        ?.map((e) =>
            e == null ? null : CellT0.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$WorldComponentsToJson(WorldComponents instance) =>
    <String, dynamic>{
      'cells0': instance.cells0,
    };

CellT0 _$CellT0FromJson(Map<String, dynamic> json) {
  return CellT0(
    json['x'] as int,
    json['y'] as int,
  );
}

Map<String, dynamic> _$CellT0ToJson(CellT0 instance) => <String, dynamic>{
      'x': instance.x,
      'y': instance.y,
    };

NaturalSize _$NaturalSizeFromJson(Map<String, dynamic> json) {
  return NaturalSize(
    json['width'] as int,
    json['height'] as int,
  );
}

Map<String, dynamic> _$NaturalSizeToJson(NaturalSize instance) =>
    <String, dynamic>{
      'width': instance.width,
      'height': instance.height,
    };
