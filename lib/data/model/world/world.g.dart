// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'world.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class WorldAdapter extends TypeAdapter<World> {
  @override
  final int typeId = 0;

  @override
  World read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return World(
      width: fields[0] as int,
      height: fields[1] as int,
      type: fields[2] as int,
    );
  }

  @override
  void write(BinaryWriter writer, World obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.width)
      ..writeByte(1)
      ..write(obj.height)
      ..writeByte(2)
      ..write(obj.type);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WorldAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

World _$WorldFromJson(Map<String, dynamic> json) {
  return World(
    id: json['id'] as String,
    createTimestamp: json['createTimestamp'] == null
        ? null
        : DateTime.parse(json['createTimestamp'] as String),
    authorId: json['authorId'] as String,
    parentId: json['parentId'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    width: json['width'] as int,
    height: json['height'] as int,
    type: json['type'] as int,
  );
}

Map<String, dynamic> _$WorldToJson(World instance) => <String, dynamic>{
      'id': instance.id,
      'createTimestamp': instance.createTimestamp?.toIso8601String(),
      'authorId': instance.authorId,
      'parentId': instance.parentId,
      'name': instance.name,
      'description': instance.description,
      'width': instance.width,
      'height': instance.height,
      'type': instance.type,
    };
