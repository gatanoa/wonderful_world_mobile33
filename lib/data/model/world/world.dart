import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'world.g.dart';

@HiveType(typeId: 0)
@JsonSerializable()
class World {
  static const empty = const World();
  final String id;
  final DateTime createTimestamp;
  final String authorId;
  final String parentId;
  final String name;
  final String description;
  @HiveField(0)
  final int width;
  @HiveField(1)
  final int height;
  @HiveField(2)
  final int type;

  const World({
    this.id,
    this.createTimestamp,
    this.authorId,
    this.parentId,
    this.name,
    this.description,
    this.width,
    this.height,
    this.type,
  });

  factory World.fromJson(Map<String, dynamic> json) => _$WorldFromJson(json);
  Map<String, dynamic> toJson() => _$WorldToJson(this);
}

class WorldTypes {
  static const kGOLb2s23MValue = 0;
  static const kGOLb3s012345678MValue = 1;
  static const kGOLb1s012345678MValue = 2;
  static const kGOLb1s012345678NValue = 3;
  static const kGOLb3678s34678MValue = 4;
  static const kBSValues = <DBValue>[
    DBValue(
      b: [3],
      s: [2, 3],
      isMoore: true,
    ),
    DBValue(
      b: [3],
      s: [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
      ],
      isMoore: true,
    ),
    DBValue(
      b: [1],
      s: [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
      ],
      isMoore: true,
    ),
    DBValue(
      b: [1],
      s: [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
      ],
      isMoore: false,
    ),
    DBValue(
      b: [
        3,
        6,
        7,
        8,
      ],
      s: [
        3,
        4,
        6,
        7,
        8,
      ],
      isMoore: true,
    ),
  ];
}

class DBValue {
  final List<int> b;
  final List<int> s;
  final bool isMoore;

  const DBValue({@required this.b, @required this.s, @required this.isMoore});
}
