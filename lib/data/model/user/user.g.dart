// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as String,
    username: json['username'] as String,
    email: json['email'] as String,
    createTimestamp: json['createTimestamp'] == null
        ? null
        : DateTime.parse(json['createTimestamp'] as String),
    createdWorldCount: json['createdWorldCount'] as int,
    selectedWorldCount: json['selectedWorldCount'] as int,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'email': instance.email,
      'createTimestamp': instance.createTimestamp?.toIso8601String(),
      'createdWorldCount': instance.createdWorldCount,
      'selectedWorldCount': instance.selectedWorldCount,
    };
