import 'package:json_annotation/json_annotation.dart';

part 'register_body.g.dart';

@JsonSerializable()
class RegisterBody {
  final String username;
  final String email;
  final String password;

  RegisterBody(this.username, this.email, this.password);

  factory RegisterBody.fromJson(Map<String, dynamic> json) =>
      _$RegisterBodyFromJson(json);
  Map<String, dynamic> toJson() => _$RegisterBodyToJson(this);
}
