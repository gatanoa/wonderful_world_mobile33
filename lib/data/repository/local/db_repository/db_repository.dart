import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_data/world_data.dart';

abstract class DbRepository {
  Future<World> loadLastWorld();
  Future<void> saveLastWorld(World world);
  Future<WorldData> loadLastWorldData();
  Future<void> saveLastWorldData(WorldComponents data);
}
