import 'package:wonderful_world/data/repository/local/db_repository/di_module.dart';
import 'package:wonderful_world/data/repository/local/settings_repository/di_module.dart';

var localRepositoryModules = [
  dbRpositoryModule,
  settingsRepositoryModule,
];
