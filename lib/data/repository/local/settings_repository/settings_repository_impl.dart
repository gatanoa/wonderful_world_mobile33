import 'package:wonderful_world/data/data_source/local/preferences/preferences_store.dart';
import 'package:wonderful_world/data/repository/local/settings_repository/settings_repository.dart';

class SettingsRepositoryImpl implements SettingsRepository {
  final PreferencesStore _prefs;

  SettingsRepositoryImpl(this._prefs);

  static const _kDarkModeStateKey = 'settings_darkmodestate';

  @override
  Future<bool> getDarkModeState() async {
    return await _prefs.getBool(_kDarkModeStateKey);
  }

  @override
  Future<void> setDarkModeState(bool state) async {
    return await _prefs.setBool(_kDarkModeStateKey, state);
  }

  static const _kReflectionBackgroundColorKey =
      'settings_reflectionbackgroundcolor';

  @override
  Future<int> getReflectionBackgroundColor() async {
    return await _prefs.getInt(_kReflectionBackgroundColorKey);
  }

  @override
  Future<void> setReflectionBackgroundColor(int color) async {
    return await _prefs.setInt(_kReflectionBackgroundColorKey, color);
  }

  static const _kReflectionCell0ColorKey = 'settings_reflectioncell0color';

  @override
  Future<int> getReflectionCell0Color() async {
    return await _prefs.getInt(_kReflectionCell0ColorKey);
  }

  @override
  Future<void> setReflectionCell0Color(int color) async {
    return await _prefs.setInt(_kReflectionCell0ColorKey, color);
  }

  static const _kReflectionGreedColorKey = 'settings_reflectiongreedcolor';

  @override
  Future<int> getReflectionGreedColor() async {
    return await _prefs.getInt(_kReflectionGreedColorKey);
  }

  @override
  Future<void> setReflectionGreedColor(int color) async {
    return await _prefs.setInt(_kReflectionGreedColorKey, color);
  }
}
