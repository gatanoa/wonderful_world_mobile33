import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/data_source/local/token_store.dart';
import 'package:wonderful_world/data/data_source/network/network_api_helper.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/data/model/register_body/register_body.dart';
import 'package:wonderful_world/data/model/login/login_body.dart';
import 'user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final NetworkApiHelper _networkApi;
  final TokenStore _tokenStore;

  BehaviorSubject<TokenUpdatingState> _tokenUpdateObservable;

  UserRepositoryImpl(this._networkApi, this._tokenStore) {
    _tokenUpdateObservable = BehaviorSubject<TokenUpdatingState>();
    _tokenStore.get().then((token) {
      var state = (token != null && token != '')
          ? TokenUpdatingState.success
          : TokenUpdatingState.failed;
      _tokenUpdateObservable.add(state);
    });
  }

  @override
  BehaviorSubject<TokenUpdatingState> get tokenUpdateSubject =>
      _tokenUpdateObservable;

  @override
  Future<bool> tokenUpdate() async {
    try {
      _tokenUpdateObservable.add(TokenUpdatingState.processing);
      var token = await _networkApi.tokenUpdate();
      await _tokenStore.set(token.access);
      await _tokenStore.setRefresh(token.refresh);
      _tokenUpdateObservable.add(TokenUpdatingState.success);
      return true;
    } on DioError catch (_) {
      _tokenUpdateObservable.add(TokenUpdatingState.failed);
      return false;
    }
  }

  @override
  Future<String> token() async {
    return await _tokenStore.get();
  }

  @override
  Future<bool> check() async {
    return await _networkApi.check();
  }

  @override
  Future<User> current() async {
    return await _networkApi.current();
  }

  @override
  Future<bool> existsEmail(String email) async {
    return await _networkApi.existsEmail(email);
  }

  @override
  Future<bool> existsUsername(String username) async {
    return await _networkApi.existsUsername(username);
  }

  @override
  Future<bool> login(LoginBody loginBody) async {
    try {
      var token = await _networkApi.login(loginBody);
      await _tokenStore.set(token.access);
      await _tokenStore.setRefresh(token.refresh);
      _tokenUpdateObservable.add(TokenUpdatingState.success);
      return true;
    } on DioError catch (_) {
      await _tokenStore.set('');
      await _tokenStore.setRefresh('');
      _tokenUpdateObservable.add(TokenUpdatingState.failed);
      return false;
    }
  }

  @override
  Future<bool> loginVk(String code) async {
    try {
      var token = await _networkApi.loginVk(code);
      await _tokenStore.set(token.access);
      await _tokenStore.setRefresh(token.refresh);
      _tokenUpdateObservable.add(TokenUpdatingState.success);
      return true;
    } on DioError catch (_) {
      await _tokenStore.set('');
      await _tokenStore.setRefresh('');
      _tokenUpdateObservable.add(TokenUpdatingState.failed);
      return false;
    }
  }

  @override
  Future<bool> logout() async {
    var result = await _networkApi.logout();
    if (result) {
      await _tokenStore.set('');
      await _tokenStore.setRefresh('');
      _tokenUpdateObservable.add(TokenUpdatingState.failed);
    }
    return result;
  }

  @override
  Future<bool> register(RegisterBody registerBody) async {
    try {
      var token = await _networkApi.register(registerBody);
      await _tokenStore.set(token.access);
      await _tokenStore.setRefresh(token.refresh);
      _tokenUpdateObservable.add(TokenUpdatingState.success);
      return true;
    } on DioError catch (_) {
      await _tokenStore.set('');
      await _tokenStore.setRefresh('');
      _tokenUpdateObservable.add(TokenUpdatingState.failed);
      return false;
    }
  }

  @override
  Future<User> user(String id) async {
    return await _networkApi.user(id);
  }

  @override
  void dispose() {
    tokenUpdateSubject.close();
  }
}
