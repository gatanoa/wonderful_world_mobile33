import 'dart:io';

import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/data_source/network/constants.dart';
import 'package:wonderful_world/data/data_source/network/token_provider.dart';

import 'user_repository/user_repository.dart';

class AuthInterceptor {
  final Future<bool> Function() _tokenUpdater;
  final BehaviorSubject<TokenUpdatingState> _tokenUpdatingSubject;
  final TokenProvider _tokenProvider;
  final Dio _dio;

  AuthInterceptor(
    this._tokenUpdater,
    this._tokenUpdatingSubject,
    this._tokenProvider,
    this._dio,
  ) {
    _dio.interceptors.add(
      InterceptorsWrapper(
        onError: _unauthorizedErrorCallback,
      ),
    );
  }

  Future<dynamic> _unauthorizedErrorCallback(DioError ex) async {
    dynamic result = ex;
    if (ex.response.statusCode == HttpStatus.unauthorized) {
      result = await _processing(
        await _tokenUpdatingSubject.first,
        ex,
      );
    }
    return result;
  }

  Future<dynamic> _processing(TokenUpdatingState state, DioError ex) async {
    if (state == TokenUpdatingState.failed ||
        ex.request.path == kPathApiAuthUpdate) {
      return ex;
    } else if (state == TokenUpdatingState.processing) {
      var newState = await _tokenUpdatingSubject
          .where(
            (event) => event != TokenUpdatingState.processing,
          )
          .first;
      return await _processing(newState, ex);
    } else if (state == TokenUpdatingState.success) {
      var request = ex.request;
      var token = await _tokenProvider.get();
      if (token == _requestToken(request.headers)) {
        if (!await _tokenUpdater()) {
          return ex;
        }
      }
      request.headers[HttpHeaders.authorizationHeader] =
          await _tokenProvider.get();
      return await _dio.request(request.path, options: request);
    }
  }

  String _requestToken(Map<String, dynamic> headers) {
    return headers[HttpHeaders.authorizationHeader];
  }
}
