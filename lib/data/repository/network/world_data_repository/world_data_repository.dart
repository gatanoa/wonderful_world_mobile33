import 'package:wonderful_world/data/model/world_data/world_data.dart';

abstract class WorldDataRepository {
  void setToken(String token);
  Future<WorldData> get(String id);
  Future<bool> save(String id, WorldComponents data);
}
