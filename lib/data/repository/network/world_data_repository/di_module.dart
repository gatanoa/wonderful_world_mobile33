import 'package:koin/koin.dart';
import 'world_data_repository.dart';
import 'world_data_repository_impl.dart';

var worldDataReposytoryModule = Module()
  ..single<WorldDataRepository>(
    (scope) => WorldDataRepositoryImpl(scope.get()),
  );
