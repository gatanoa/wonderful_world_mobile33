import 'dart:io';

import 'package:dio/dio.dart';
import 'package:wonderful_world/data/data_source/network/token_provider.dart';
import 'package:wonderful_world/data/data_source/network/world_data/world_data_network_api.dart';
import 'package:wonderful_world/data/model/world_data/world_data.dart';
import 'package:wonderful_world/data/repository/network/world_data_repository/world_data_repository.dart';

class WorldDataRepositoryImpl implements WorldDataRepository {
  final WorldDataNetworkApi _networkApi;
  String token;

  WorldDataRepositoryImpl(this._networkApi);

  @override
  Future<WorldData> get(String id) async {
    return await _networkApi.getWorldData(id);
  }

  @override
  Future<bool> save(String id, WorldComponents data) async {
    try {
      await _networkApi.saveWorldData(token, id, data);
      return true;
    } on DioError catch (ex) {
      if (ex.response.statusCode == HttpStatus.notFound) {
        return false;
      } else {
        throw ex;
      }
    }
  }

  @override
  void setToken(String token) {
    this.token = TokenProvider.tokenTokHeaderRow(token);
  }
}
