import 'package:koin/koin.dart';
import 'package:wonderful_world/data/repository/local/settings_repository/di_module.dart';
import 'auth_interceptor.dart';
import 'user_repository/di_module.dart';
import 'world_repository/di_module.dart';

import 'user_repository/user_repository.dart';

var networkRepositoryModules = [
  settingsRepositoryModule,
  userRepositoryModule,
  worldRepositoryModule,
  Module()
    ..single(
      (scope) => AuthInterceptor(
        scope.get<UserRepository>().tokenUpdate,
        scope.get<UserRepository>().tokenUpdateSubject,
        scope.get(),
        scope.get(),
      ),
      createdAtStart: true,
    ),
];
