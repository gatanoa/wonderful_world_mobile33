import 'package:wonderful_world/data/data_source/network/network_api_helper.dart';
import 'package:wonderful_world/data/model/world_add_req_body/world_add_req_body.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'world_repository.dart';

class WorldRepositoryImpl implements WorldRepository {
  final NetworkApiHelper _networkApi;

  const WorldRepositoryImpl(this._networkApi);

  @override
  Future<bool> add(WorldAddReqBody world) async {
    return await _networkApi.add(world);
  }

  @override
  Future<bool> clone(String id) async {
    return await _networkApi.clone(id);
  }

  @override
  Future<List<String>> createdWorlds() async {
    return await _networkApi.createdWorlds();
  }

  @override
  Future<bool> remove(String id) async {
    return await _networkApi.remove(id);
  }

  @override
  Future<List<String>> search({
    String name,
    String authorId,
    String authorName,
  }) async {
    return await _networkApi.search(
      name: name,
      authorId: authorId,
      authorName: authorName,
    );
  }

  @override
  Future<bool> selectedWorldAdd(String id) async {
    return await _networkApi.selectedWorldAdd(id);
  }

  @override
  Future<bool> selectedWorldRemove(String id) async {
    return await _networkApi.selectedWorldRemove(id);
  }

  @override
  Future<List<String>> selectedWorlds() async {
    return await _networkApi.selectedWorlds();
  }

  @override
  Future<World> world(String id) async {
    return await _networkApi.world(id);
  }

  @override
  Future<List<String>> worlds() async {
    return await _networkApi.worlds();
  }
}
