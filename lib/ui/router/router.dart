import 'package:auto_route/auto_route_annotations.dart';
import 'package:wonderful_world/ui/screen/create_world/createworld_screen.dart';
import 'package:wonderful_world/ui/screen/current_user/currentuser_screen.dart';
import 'package:wonderful_world/ui/screen/login_cwelt/logincwelt_screen.dart';
import 'package:wonderful_world/ui/screen/login/login_screen.dart';
import 'package:wonderful_world/ui/screen/login_vk/loginvk_screen.dart';
import 'package:wonderful_world/ui/screen/reflection/reflection_screen.dart';
import 'package:wonderful_world/ui/screen/search_worlds/searchworlds_screen.dart';
import 'package:wonderful_world/ui/screen/start/start_screen.dart';
import 'package:wonderful_world/ui/screen/home/home_screen.dart';
import 'package:wonderful_world/ui/screen/user/user_screen.dart';
import 'package:wonderful_world/ui/screen/world/world_screen.dart';

@MaterialAutoRouter(routes: <AutoRoute>[
  MaterialRoute(page: StartScreen, initial: true),
  MaterialRoute(page: HomeScreen),
  MaterialRoute(page: LoginCWeltScreen),
  MaterialRoute(page: LoginVkScreen),
  MaterialRoute(page: LoginScreen),
  MaterialRoute(page: CreateworldScreen),
  MaterialRoute(page: CurrentuserScreen),
  MaterialRoute(page: SearchworldsScreen),
  MaterialRoute(page: WorldScreen),
  MaterialRoute(page: UserScreen),
  MaterialRoute(page: ReflectionScreen),
])
class $Router {}
