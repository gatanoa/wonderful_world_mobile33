part of 'worldlist_cubit.dart';

@immutable
abstract class WorldlistState {
  final List<World> items;

  WorldlistState({this.items = const <World>[]});
}

class WorldslistInitial extends WorldlistState {}

class WorldslistLoaded extends WorldlistState {
  WorldslistLoaded(List<World> items): super(items: items);
}

class WorldslistFail extends WorldlistState {}
