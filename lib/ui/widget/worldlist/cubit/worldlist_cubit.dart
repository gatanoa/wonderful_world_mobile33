import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/domain/world_center.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';

part 'worldlist_state.dart';

abstract class WorldlistCubit extends Cubit<WorldlistState> {
  final WorldCenter worldCenter;
  StreamSubscription<void> _worldsChangeSubscription;

  WorldlistCubit(
    this.worldCenter,
  ) : super(WorldslistInitial()) {
    _worldsChangeSubscription =
        worldCenter.worldsChangeObservable.listen(_worldsChangedListener);
  }

  Future<void> update() async {
    try {
      var worlds = await getWorlds();
      emit(WorldslistLoaded(worlds));
    } catch (_) {
      emit(WorldslistFail());
    }
  }

  void _worldsChangedListener(_) {
    this.update();
  }

  Future<List<World>> getWorlds();

  @override
  Future<void> close() async {
    await _worldsChangeSubscription.cancel();
    await super.close();
  }

  void itemPressedListener(World world) {
    ExtendedNavigator.root.push(
      Routes.worldScreen,
      arguments: WorldScreenArguments(world: world),
    );
  }
}
