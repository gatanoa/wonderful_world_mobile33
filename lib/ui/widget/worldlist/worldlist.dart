import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/data/model/world/world.dart';

import '../awaiting.dart';
import '../popup_menu.dart';
import 'cubit/worldlist_cubit.dart';

class WorldList<T extends WorldlistCubit> extends StatefulWidget {
  final int contextMenuButtons; // aka ButtonType
  final T cubit;
  final Widget head;

  const WorldList({Key key, this.contextMenuButtons, this.cubit, this.head})
      : super(key: key);
  @override
  _WorldListState<T> createState() => _WorldListState<T>();
}

class _WorldListState<T extends WorldlistCubit> extends State<WorldList<T>>
    with ScopeStateMixin {
  final ScrollController _scrollController = ScrollController();
  T _cubit;
  PopupMenu _popupMenu;
  List<World> _items;
  S _l10n;

  @override
  void initState() {
    super.initState();
    _cubit = widget.cubit ?? get();
    _popupMenu = get();
    _cubit.update();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<T, WorldlistState>(
      cubit: _cubit,
      builder: (context, state) {
        var result;
        if (state is WorldslistInitial) {
          result = Awaiting();
        } else if (state is WorldslistLoaded) {
          var items = state.items;
          if (_items != items) {
            _items = items;
          }
          result = DraggableScrollbar.arrows(
            controller: _scrollController,
            scrollbarTimeToFade: const Duration(seconds: 3),
            child: ListView.builder(
              controller: _scrollController,
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: (widget.head == null) ? _items.length : _items.length + 1,
              itemBuilder: _headerBuilder,
            ),
          );
        } else {
          result = ListView();
        }
        return RefreshIndicator(
          child: result,
          onRefresh: _cubit.update,
        );
      },
    );
  }

  Widget _headerBuilder(BuildContext context, int index) {
    if (widget.head == null) {
      return _listItemBuilder(context, index);
    } else if (index > 0) {
      return _listItemBuilder(context, index - 1);
    } else {
      return widget.head;
    }
  }

  Widget _listItemBuilder(BuildContext context, int index) {
    var item = _items[index];
    var typeName;
    if (item.type == WorldTypes.kGOLb2s23MValue) {
      typeName = _l10n.cell_type_0;
    } else if (item.type == WorldTypes.kGOLb3s012345678MValue) {
      typeName = _l10n.cell_type_1;
    } else if (item.type == WorldTypes.kGOLb1s012345678MValue) {
      typeName = _l10n.cell_type_2;
    } else if (item.type == WorldTypes.kGOLb1s012345678NValue) {
      typeName = _l10n.cell_type_3;
    } else if (item.type == WorldTypes.kGOLb3678s34678MValue) {
      typeName = _l10n.cell_type_4;
    } else {
      typeName = _l10n.cell_type_99;
    }
    return Container(
        decoration: BoxDecoration(
          border: Border(bottom: Divider.createBorderSide(context)),
        ),
        child: GestureDetector(
          onLongPressStart: (details) =>
              itemLongPressStartListener(details, item.id),
          child: ListTile(
              title: Row(
                children: [
                  Expanded(
                    child: Text(item.name, overflow: TextOverflow.ellipsis),
                  ),
                  Text('(${item.width} x ${item.height})'),
                ],
              ),
              trailing: Icon(Icons.keyboard_arrow_right),
              subtitle: Text(
                typeName,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              onTap: () => _cubit.itemPressedListener(item)),
        ));
  }

  void itemLongPressStartListener(
    LongPressStartDetails details,
    String worldId,
  ) {
    _popupMenu.show(
        context: context,
        details: details,
        worldId: worldId,
        buttonTypes: widget.contextMenuButtons);
  }
}
