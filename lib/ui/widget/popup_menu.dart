import 'package:flutter/material.dart';
import 'package:wonderful_world/domain/user_center.dart';
import 'package:wonderful_world/domain/world_center.dart';
import 'package:wonderful_world/generated/l10n.dart';

class PopupMenu {
  static const int APP_BAR_POPUP_SELECTED_ADD_VALUE = 1;
  static const int APP_BAR_POPUP_SELECTED_REMOVE_VALUE = 2;
  static const int APP_BAR_POPUP_REMOVE_VALUE = 3;
  static const int APP_BAR_POPUP_CLONE_VALUE = 4;

  final UserCenter _userCenter;
  final WorldCenter _worldCenter;

  PopupMenu(this._userCenter, this._worldCenter);

  Future<List<PopupMenuItem>> getPopupItems({
    @required BuildContext context,
    @required String id,
    bool isAuth = false,
    int buttonTypes = ButtonType.empty,
  }) async {
    var l10n = S.of(context);
    List<PopupMenuItem> result = [];

    if (id != null) {
      if (await _userCenter.isAuth()) {
        if (ButtonType.isSelect(buttonTypes)) {
          if (await _worldCenter.selectedWorldsContains(id)) {
            result.add(PopupMenuItem(
                value: APP_BAR_POPUP_SELECTED_REMOVE_VALUE,
                child: Text(l10n.contextMenu_item_selected_remove)));
          } else {
            result.add(PopupMenuItem(
                value: APP_BAR_POPUP_SELECTED_ADD_VALUE,
                child: Text(l10n.contextMenu_item_selected_add)));
          }
        }
        if (ButtonType.isClone(buttonTypes)) {
          result.add(PopupMenuItem(
              value: APP_BAR_POPUP_CLONE_VALUE,
              child: Text(l10n.contextMenu_item_clone)));
        }
        if (ButtonType.isRemove(buttonTypes)) {
          result.add(PopupMenuItem(
              value: APP_BAR_POPUP_REMOVE_VALUE,
              child: Text(l10n.contextMenu_item_remove)));
        }
      }
    }

    return result;
  }

  Future<void> show({
    @required BuildContext context,
    @required LongPressStartDetails details,
    @required String worldId,
    int buttonTypes = ButtonType.empty,
  }) async {
    var x = details.globalPosition.dx;
    var y = details.globalPosition.dy;
    var pos = RelativeRect.fromLTRB(x, y, x + 1, y + 1);
    if (await _userCenter.isAuth()) {
      return await showMenu(
              context: context,
              position: pos,
              items: await getPopupItems(
                context: context,
                id: worldId,
                buttonTypes: buttonTypes,
              ))
          .then((value) => listener(value, context, worldId));
    }
    return null;
  }

  void listener(int value, BuildContext context, String worldId) {
    if (value == APP_BAR_POPUP_SELECTED_ADD_VALUE) {
      _selectedWorldAdd(context, worldId);
    } else if (value == APP_BAR_POPUP_SELECTED_REMOVE_VALUE) {
      _selectedWorldRemove(context, worldId);
    } else if (value == APP_BAR_POPUP_REMOVE_VALUE) {
      _worldRemove(context, worldId);
    } else if (value == APP_BAR_POPUP_CLONE_VALUE) {
      _worldClone(context, worldId);
    }
  }

  void _selectedWorldAdd(BuildContext context, String worldId) {
    _worldCenter.selectedWorldAdd(worldId);
  }

  void _selectedWorldRemove(BuildContext context, String worldId) {
    _worldCenter.selectedWorldRemove(worldId);
  }

  void _worldClone(BuildContext context, String worldId) {
    _worldCenter.clone(worldId);
  }

  void _worldRemove(BuildContext context, String worldId) {
    _worldCenter.remove(worldId);
  }
}

class ButtonType {
  static const int empty = 0x00;

  static const int select = 0x01;
  static const int remove = 0x02;
  static const int clone = 0x04;

  static const int all = select | remove | clone;

  static bool isSelect(int value) => (value & select) == select;

  static bool isRemove(int value) => (value & remove) == remove;

  static bool isClone(int value) => (value & clone) == clone;
}
