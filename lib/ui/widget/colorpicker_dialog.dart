import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:wonderful_world/generated/l10n.dart';

import 'singlechildscrollbarview.dart';

class ColorpickerDialog {
  static Future<Color> show(BuildContext context, Color selectedColor) async {
    Color bufferColor;
    if (await showDialog(
        context: context,
        child: AlertDialog(
          content: SingleChildScrollBarView(
            child: ColorPicker(
              pickerColor: selectedColor,
              onColorChanged: (value) {
                bufferColor = value;
              },
            ),
          ),
          actions: [
            FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text(S.of(context).button_ok)),
            FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(S.of(context).button_cancel))
          ],
        ))) {
      return bufferColor;
    } else {
      return selectedColor;
    }
  }
}
