import 'package:flutter/material.dart';

class AutodropdownButton<T> extends StatefulWidget {
  final List<DropdownMenuItem<T>> items;
  final ValueChanged<T> onChanged;
  final T initValue;

  const AutodropdownButton({
    Key key,
    this.items,
    this.onChanged,
    this.initValue,
  }) : super(key: key);

  @override
  _AutodropdownButtonState<T> createState() =>
      _AutodropdownButtonState<T>(initValue);
}

class _AutodropdownButtonState<T> extends State<AutodropdownButton<T>> {
  T _value;

  _AutodropdownButtonState(T initValue) : _value = initValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<T>(
      isExpanded: true,
      value: _value,
      items: widget.items,
      onChanged: _onChanged,
    );
  }

  void _onChanged(T value) {
    widget.onChanged(value);
    setState(() {
      _value = value;
    });
  }
}
