import 'package:flutter/material.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';

class ButtonsBar extends StatelessWidget {
  final String actionTitle;
  final String cancelTitle;
  final VoidCallback actionPressed;
  final VoidCallback cancelPressed;
  final Widget actionButton;

  const ButtonsBar({
    Key key,
    this.actionTitle,
    this.cancelTitle,
    this.actionPressed,
    this.cancelPressed,
    this.actionButton,
  })  : assert(
          actionButton == null ||
              (actionTitle == null && actionPressed == null),
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var l10n = S.of(context);

    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: Divider.createBorderSide(context),
        ),
      ),
      padding: Indents.kDefaultHorizontalIndention,
      child: Row(
        children: [
          Expanded(
            child: actionButton ??
                FlatButton(
                  onPressed:
                      actionPressed ?? () => Navigator.pop(context, true),
                  child: Text(actionTitle ?? l10n.button_ok),
                ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: FlatButton(
              onPressed: cancelPressed ?? () => Navigator.pop(context, false),
              child: Text(cancelTitle ?? l10n.button_cancel),
            ),
          ),
        ],
      ),
    );
  }
}
