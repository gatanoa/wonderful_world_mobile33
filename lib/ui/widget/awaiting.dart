import 'package:flutter/material.dart';

class Awaiting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Transform.scale(
        scale: 2,
        child: const CircularProgressIndicator(),
      ),
    );
  }
}