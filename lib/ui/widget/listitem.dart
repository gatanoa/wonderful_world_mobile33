import 'package:flutter/material.dart';
import 'package:wonderful_world/ui/constants/indents.dart';

class ListItem extends StatelessWidget {
  final String title;
  final String description;
  final VoidCallback onDescriptionPressed;
  final Icon leading;
  final Icon trailing;

  const ListItem({
    Key key,
    this.title,
    this.description,
    this.onDescriptionPressed,
    this.leading,
    this.trailing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var topElementList = <Widget>[
      leading ??
          Text(
            title ?? '',
            style: theme.textTheme.caption,
          ),
    ];

    Widget bottomItem = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        if (onDescriptionPressed == null)
          Text(description ?? '')
        else
          Text(
            '[ ${description ?? ''} ]',
            style: TextStyle(color: theme.buttonColor),
          ),
        if (trailing != null) trailing,
      ],
    );

    if (onDescriptionPressed != null) {
      bottomItem = GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: onDescriptionPressed,
        child: bottomItem,
      );
    }

    return Card(
      margin: Indents.kCardIndention,
      elevation: 4,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 10, 16, 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: topElementList),
            Divider(),
            bottomItem,
          ],
        ),
      ),
    );
  }
}
