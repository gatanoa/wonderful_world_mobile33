import 'package:koin/koin.dart';
import 'package:wonderful_world/ui/widget/popup_menu.dart';

var widgetModule = Module()
  ..single((scope) => PopupMenu(scope.get(), scope.get()));
