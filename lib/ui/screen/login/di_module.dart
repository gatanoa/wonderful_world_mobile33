import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'package:wonderful_world/ui/screen/login/login_screen.dart';
import 'cubit/loginscreen_cubit.dart';

var loginModule = Module()
  ..scopeOneCubit<LoginscreenCubit, LoginScreen>(
    (scope) => LoginscreenCubit(),
  );
