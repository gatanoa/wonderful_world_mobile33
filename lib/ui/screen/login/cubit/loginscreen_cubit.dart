import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';

part 'loginscreen_state.dart';

class LoginscreenCubit extends Cubit<LoginscreenState> {
  LoginscreenCubit() : super(LoginscreenInitial());

  void cweltLoginPressedListener() {
    ExtendedNavigator.root.push(Routes.loginCWeltScreen).then((value) {
      if (value == true) {
        ExtendedNavigator.root.pop();
      }
    });
  }

  void vkLoginPressedListener() {
    ExtendedNavigator.root.push(Routes.loginVkScreen).then((value) {
      if (value == true) {
        ExtendedNavigator.root.pop();
      }
    });
  }
}
