import 'package:flutter/material.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/screen/login/cubit/loginscreen_cubit.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  S _l10n;
  LoginscreenCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_l10n.loginScreen_title_logIn),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: Indents.kCardIndention,
              child: Row(
                children: [
                  Expanded(
                    child: RaisedButton(
                      onPressed: _cubit.cweltLoginPressedListener,
                      child: Text('Войти через CWelt'),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: Indents.kCardIndention,
              child: Row(
                children: [
                  Expanded(
                    child: RaisedButton(
                      onPressed: _cubit.vkLoginPressedListener,
                      child: Text('Войти через ВКонтакте'),
                      color: Color.fromRGBO(81, 129, 184, 1.0),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
