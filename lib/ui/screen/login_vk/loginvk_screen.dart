import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:wonderful_world/ui/screen/login_vk/constanrs.dart';
import 'package:wonderful_world/ui/screen/login_vk/cubit/loginvkscreen_cubit.dart';
import 'package:wonderful_world/ui/widget/awaiting.dart';

class LoginVkScreen extends StatefulWidget {
  @override
  _LoginVkScreenState createState() => _LoginVkScreenState();
}

class _LoginVkScreenState extends State<LoginVkScreen> with ScopeStateMixin {
  LoginvkscreenCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      cubit: _cubit,
      builder: (context, state) {
        Widget body;
        if (state is LoginvkscreenInitial) {
          body = WebView(
            initialUrl: kOauthVkUrl,
            navigationDelegate: _cubit.redirectListener,
          );
        } else {
          // state is LoginvkscreenAwait
          body = Awaiting();
        }
        return Scaffold(
          appBar: AppBar(
            title: Text('Вход через Вконтакте'),
          ),
          body: body,
        );
      },
    );
  }
}
