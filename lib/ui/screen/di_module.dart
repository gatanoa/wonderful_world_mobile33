import 'create_world/di_module.dart';
import 'current_user/di_module.dart';
import 'home/ui_module.dart';
import 'login_cwelt/di_module.dart';
import 'login/di_module.dart';
import 'login_vk/di_module.dart';
import 'search_worlds/di_module.dart';
import 'start/di_module.dart';
import 'world/di_module.dart';
import 'reflection/di_module.dart';

var screenModules = [
  startscreenModule,
  homescreenModule,
  loginModule,
  loginCWeltModule,
  loginVkModule,
  createworldModule,
  currentuserModule,
  searchworldsModule,
  worldModule,
  ...reflectionModules,
];
