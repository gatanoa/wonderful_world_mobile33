part of 'homescreen_cubit.dart';

@immutable
abstract class HomescreenState {
  final int index;
  final bool isAuth;
  final String username;

  HomescreenState(this.index, this.isAuth, {this.username});
}

class HomescreenAvailable extends HomescreenState {
  HomescreenAvailable(bool isAuth, {String username})
      : super(0, isAuth, username: username);
}

class HomescreenSelected extends HomescreenState {
  HomescreenSelected(bool isAuth, {String username})
      : super(1, isAuth, username: username);
}

class HomescreenCreated extends HomescreenState {
  HomescreenCreated(bool isAuth, {String username})
      : super(2, isAuth, username: username);
}
