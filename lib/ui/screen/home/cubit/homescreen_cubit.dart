import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/domain/user_center.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';

part 'homescreen_state.dart';

class HomescreenCubit extends Cubit<HomescreenState> {
  final UserCenter _userCenter;

  int _pos;
  PageController _pageController;
  PageController get pageController => _pageController;
  StreamSubscription<bool> _authStateChangeSubscription;

  HomescreenCubit(this._userCenter)
      : super(HomescreenAvailable(false)) {
      _pos = 0;
      _pageController = PageController(initialPage: _pos);
      _pageController.addListener(_scrollListener);
    _authStateChangeSubscription =
        _userCenter.authStateChangeObservable.listen(_authStateChangedListener);
    _userCenter.isAuth().then((isAuth) {
      if (isAuth) {
        _userCenter.current().then((user) {
          emit(HomescreenAvailable(true, username: user.username));
          if (user.selectedWorldCount > 0) {
            _scrollTo(1);
          }
        }).catchError((_) {
          emit(HomescreenAvailable(false));
        });
      } else {
        emit(HomescreenAvailable(false));
      }
    });
  }

  void _authStateChangedListener(bool isAuth) {
    if (isAuth) {
      _userCenter.current().then((user) {
        _authStateChange(true, user.username);
      }).catchError((_) {
        _authStateChange(false, '');
        _scrollTo(0);
      });
    } else {
      _authStateChange(false, '');
      _scrollTo(0);
    }
  }

  void _authStateChange(bool isAuth, String username) {
    if (state is HomescreenAvailable) {
      emit(HomescreenAvailable(isAuth, username: username));
    } else if (state is HomescreenSelected) {
      emit(HomescreenSelected(isAuth, username: username));
    } else if (state is HomescreenCreated) {
      emit(HomescreenCreated(isAuth, username: username));
    }
  }

  void setAvailable() {
    emit(HomescreenAvailable(state.isAuth, username: state.username));
  }

  void setSelected() {
    emit(HomescreenSelected(state.isAuth, username: state.username));
  }

  void setCreated() {
    emit(HomescreenCreated(state.isAuth, username: state.username));
  }

  void draverLinkTapListener(int page) {
    ExtendedNavigator.root.pop();
    _scrollTo(page);
  }

  void _scrollTo(int page) {
    if (_pos != page) {
      _pageController.animateToPage(
        page,
        duration: kTabScrollDuration,
        curve: Curves.easeIn,
      );
    }
  }

  void userCardPressedListener() {
    ExtendedNavigator.root.pop();
    if (this.state.isAuth) {
      ExtendedNavigator.root.push(Routes.currentuserScreen);
    } else {
      ExtendedNavigator.root.push(Routes.loginScreen);
    }
  }

  void _scrollListener() {
    var position = _pageController.page.round();
    if (position != _pos) {
      _pos = position;
      set(_pos);
    }
  }

  void set(int index) {
    switch (index) {
      case 0:
        setAvailable();
        break;
      case 1:
        setSelected();
        break;
      case 2:
        setCreated();
        break;
    }
  }

  @override
  Future<void> close() async {
    _pageController.dispose();
    _authStateChangeSubscription.cancel();
    await super.close();
  }
}
