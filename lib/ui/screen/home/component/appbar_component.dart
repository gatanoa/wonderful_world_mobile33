import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';
import 'package:wonderful_world/ui/screen/home/cubit/homescreen_cubit.dart';

class AppBarComponent extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomescreenCubit, HomescreenState>(
      builder: (context, state) {
        var l10n = S.of(context);
        var title;
        var actions;

        if (state is HomescreenAvailable) {
          title = l10n.homeScreen_title_available;
          actions = <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: _searchPressedListener,
            ),
          ];
        } else if (state is HomescreenSelected) {
          title = l10n.homeScreen_title_selected;
        } else if (state is HomescreenCreated) {
          title = l10n.homeScreen_title_created;
          actions = <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: _createPressedListener,
            ),
          ];
        }

        return AppBar(
          title: Text(title),
          actions: actions,
        );
      },
    );
  }

  void _searchPressedListener() {
    ExtendedNavigator.root.push(Routes.searchworldsScreen);
  }

  void _createPressedListener() {
    ExtendedNavigator.root.push(Routes.createworldScreen);
  }
}
