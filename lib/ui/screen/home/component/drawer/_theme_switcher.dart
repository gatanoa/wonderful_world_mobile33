part of 'drawer_component.dart';

class _ThemeSwitcher extends StatefulWidget {
  @override
  _ThemeSwitcherState createState() => _ThemeSwitcherState();
}

class _ThemeSwitcherState extends State<_ThemeSwitcher> with ScopeStateMixin {
  ThemeCubit _cubit;
  S _l10n;

  @override
  void initState() {
    super.initState();
    _cubit = get();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: Divider.createBorderSide(context),
        ),
      ),
      padding: Indents.kDefaultLeftIndention +
          const EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        children: [
          Expanded(
            child: Text(_l10n.homeScreen_drawer_darkTheme),
          ),
          Switch(
            value: _cubit.state.isDark,
            onChanged: (_) => _onSwitch(),
          ),
        ],
      ),
    );
  }

  Future<void> _onSwitch() async {
    _cubit.toSwitch();
  }
}