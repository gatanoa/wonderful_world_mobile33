import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/screen/home/component/appbar_component.dart';
import 'package:wonderful_world/ui/screen/home/component/drawer/drawer_component.dart';
import 'package:wonderful_world/ui/widget/popup_menu.dart';
import 'package:wonderful_world/ui/widget/worldlist/worldlist.dart';

import 'component/body/cubit/available_worlds_body_cubit.dart';
import 'component/body/cubit/created_worlds_body_cubit.dart';
import 'component/body/cubit/selected_worlds_body_cubit.dart';
import 'cubit/homescreen_cubit.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomescreenCubit _cubit;
  S _l10n;

  List<LinkItem> _menuItems;

  @override
  void initState() {
    super.initState();
    _cubit = get();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
    _menuItems = <LinkItem>[
      LinkItem(0, _l10n.homeScreen_title_available, false),
      LinkItem(1, _l10n.homeScreen_title_selected, true),
      LinkItem(2, _l10n.homeScreen_title_created, true),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _cubit,
      child: Scaffold(
        appBar: AppBarComponent(),
        drawer: DrawerComponent(
          items: _menuItems,
        ),
        body: BlocBuilder<HomescreenCubit, HomescreenState>(
          builder: (_, state) {
            return PageView(
              physics: (state.isAuth) ? null : NeverScrollableScrollPhysics(),
              controller: _cubit.pageController,
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                WorldList<AvailableworldsbodyCubit>(
                  contextMenuButtons: ButtonType.select,
                ),
                WorldList<SelectedworldsbodyCubit>(
                  contextMenuButtons: ButtonType.select,
                ),
                WorldList<CreatedworldsbodyCubit>(
                  contextMenuButtons: ButtonType.all,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
