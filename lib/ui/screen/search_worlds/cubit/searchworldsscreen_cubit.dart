import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/domain/world_center.dart';
import 'package:wonderful_world/ui/widget/worldlist/cubit/worldlist_cubit.dart';

class SearchworldsscreenCubit extends WorldlistCubit {
  SearchParameter param;

  SearchworldsscreenCubit(WorldCenter worldCenter) : super(worldCenter);

  @override
  Future<List<World>> getWorlds() {
    return worldCenter.search(param?.name, param?.authorId, param?.authorName);
  }

  void search(SearchParameter param) {
    this.param = param;
    this.update();
  }

  void clear() {
    this.param = SearchParameter();
  }
}

class SearchParameter {
  static const SearchParameter empty = const SearchParameter._();

  final String name;
  final String authorName;
  final String authorId;

  const SearchParameter._({
    this.name,
    this.authorName,
    this.authorId,
  });

  factory SearchParameter({
    String name,
    String authorName,
    String authorId,
  }) {
    var bufferName = (name != null && name.isEmpty) ? null : name;
    var bufferAuthorName =
        (authorName != null && authorName.isEmpty) ? null : authorName;

    return SearchParameter._(
      name: bufferName,
      authorName: bufferAuthorName,
      authorId: authorId,
    );
  }
}
