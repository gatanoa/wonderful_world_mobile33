import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';

import 'cubit/searchworldsscreen_cubit.dart';
import 'searchworlds_screen.dart';

var searchworldsModule = Module()
  ..scopeOneCubit<SearchworldsscreenCubit, SearchworldsScreen>(
    (scope) => SearchworldsscreenCubit(scope.get()),
  );
