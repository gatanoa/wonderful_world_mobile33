part of 'logincweltscreen_cubit.dart';

@immutable
abstract class LogincweltscreenState {}

class LogincweltscreenInitial extends LogincweltscreenState {}

class LogincweltscreenUsernameExists extends LogincweltscreenState {}

class LogincweltscreenEmailExists extends LogincweltscreenState {}

class LogincweltscreenCorrect extends LogincweltscreenState {}

class LogincweltscreenAwait extends LogincweltscreenState {}
