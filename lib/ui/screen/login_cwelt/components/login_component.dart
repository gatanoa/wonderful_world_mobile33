import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/constants/regexps.dart';
import 'package:wonderful_world/ui/constants/textinput_formatters.dart';
import 'package:wonderful_world/ui/widget/awaiting_button.dart';

import '../cubit/logincweltscreen_cubit.dart';

class LoginComponent extends StatefulWidget {
  @override
  LoginComponentState createState() => LoginComponentState();
}

class LoginComponentState extends State<LoginComponent> with ScopeStateMixin {
  S _l10n;
  LogincweltscreenCubit _loginscreenCubit;

  bool _isEmailValidate = false;
  bool _isPasswordValidate = false;

  String _email;
  String _password;

  @override
  void initState() {
    super.initState();
    _loginscreenCubit = currentScope.get();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _loginscreenCubit,
      child: Column(
        children: [
          Padding(
            padding: Indents.kCardIndention,
            child: TextFormField(
              autovalidateMode: AutovalidateMode.always,
              validator: _emailValidator,
              inputFormatters: TextInputFormatters.fEmail,
              decoration: InputDecoration(
                hintText: _l10n.loginScreen_input_email,
                labelText: _l10n.loginScreen_input_email,
              ),
            ),
          ),
          Padding(
            padding: Indents.kCardIndention,
            child: TextFormField(
              obscureText: true,
              autovalidateMode: AutovalidateMode.always,
              validator: _passwordValidator,
              inputFormatters: TextInputFormatters.fBaseName,
              decoration: InputDecoration(
                hintText: _l10n.loginScreen_input_password,
                labelText: _l10n.loginScreen_input_password,
              ),
            ),
          ),
          Padding(
            padding: Indents.kCardIndention,
            child: Row(
              children: [
                Expanded(
                  child: AwaitingButton<LogincweltscreenCubit, LogincweltscreenState>(
                    title: _l10n.loginScreen_button_logIn,
                    onPressed: _onLoginPressedListener,
                    isEnabledDelegat: (state) => state is LogincweltscreenCorrect,
                    isAwaitDelegat: (state) => state is LogincweltscreenAwait,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _emailValidator(String value) {
    var result;
    var stepValidate =
        value != null && value.isNotEmpty && RegExps.fEmail.hasMatch(value);
    if (!stepValidate) {
      result = _l10n.loginScreen_input_email_validate_fail;
    }
    _email = value;
    if (_isEmailValidate != stepValidate || stepValidate) {
      _isEmailValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  String _passwordValidator(String value) {
    var result;
    var stepValidate = value != null && value.isNotEmpty;
    if (!stepValidate) {
      result = _l10n.loginScreen_input_password_validate_fail;
    }
    _password = value;
    if (_isPasswordValidate != stepValidate || stepValidate) {
      _isPasswordValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  bool get isAllValidate => _isEmailValidate && _isPasswordValidate;

  Future<void> _changedListener() async {
    if (isAllValidate) {
      _loginscreenCubit.setCorrect();
      return;
    }

    if (_loginscreenCubit.state is LogincweltscreenCorrect) {
      _loginscreenCubit.setDefault();
    }
  }

  void _onLoginPressedListener() {
    if (isAllValidate) {
      _loginscreenCubit.login(_email, _password);
    }
  }
}
