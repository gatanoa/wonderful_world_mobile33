import 'package:flutter/material.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/widget/singlechildscrollbarview.dart';

import 'components/login_component.dart';
import 'components/signup_component.dart';

class LoginCWeltScreen extends StatefulWidget {
  @override
  _LoginCWeltScreenState createState() => _LoginCWeltScreenState();
}

class _LoginCWeltScreenState extends State<LoginCWeltScreen> {
  S _l10n;
  ButtonThemeData _buttonTheme;
  bool _isLogIn = true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
    _buttonTheme = ButtonTheme.of(context);
  }

  @override
  Widget build(BuildContext context) {
    var title;
    var linkTitle;
    Widget body;
    if (_isLogIn) {
      title = _l10n.loginScreen_title_logIn;
      linkTitle = _l10n.loginScreen_link_signUp;
      body = LoginComponent();
    } else {
      title = _l10n.loginScreen_title_signUp;
      linkTitle = _l10n.loginScreen_link_logIn;
      body = SignupComponent();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollBarView(
        child: Container(
          child: body,
          margin: EdgeInsets.only(bottom: _buttonTheme.height) +
              Indents.kDefaultBottomIndention,
        ),
      ),
      bottomSheet: GestureDetector(
        onTap: () => setState(() => _isLogIn = !_isLogIn),
        child: Container(
          decoration: BoxDecoration(
            border: Border(top: Divider.createBorderSide(context)),
          ),
          height: _buttonTheme.height,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text(linkTitle)],
          ),
        ),
      ),
    );
  }
}