import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'components/login_component.dart';
import 'components/signup_component.dart';
import 'cubit/logincweltscreen_cubit.dart';

var loginCWeltModule = Module()
  ..scopeOneCubit<LogincweltscreenCubit, LoginComponent>(
    (scope) => LogincweltscreenCubit(scope.get()),
  )
  ..scopeOneCubit<LogincweltscreenCubit, SignupComponent>(
    (scope) => LogincweltscreenCubit(scope.get()),
  );
