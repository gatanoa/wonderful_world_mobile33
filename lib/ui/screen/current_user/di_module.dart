import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'cubit/currentuser_cubit.dart';
import 'currentuser_screen.dart';

var currentuserModule = Module()
  ..scopeOneCubit<CurrentuserscreenCubit, CurrentuserScreen>(
    (scope) => CurrentuserscreenCubit(scope.get()),
  );
