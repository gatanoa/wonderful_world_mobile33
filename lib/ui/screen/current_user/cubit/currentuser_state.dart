part of 'currentuser_cubit.dart';

@immutable
abstract class CurrentuserscreenState {
  final User user;

  CurrentuserscreenState({this.user});
  }

class CurrentuserscreenInitial extends CurrentuserscreenState {}

class CurrentuserscreenLoaded extends CurrentuserscreenState {
  CurrentuserscreenLoaded(User user): super(user: user);
}

class CurrentuserscreenFail extends CurrentuserscreenState {}
