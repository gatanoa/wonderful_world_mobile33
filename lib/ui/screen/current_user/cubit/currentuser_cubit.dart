import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/domain/user_center.dart';

part 'currentuser_state.dart';

class CurrentuserscreenCubit extends Cubit<CurrentuserscreenState> {
  final UserCenter _userCenter;
  CurrentuserscreenCubit(this._userCenter) : super(CurrentuserscreenInitial()) {
    update();
  }

  void update() {
    _userCenter.current().then((user) {
      emit(CurrentuserscreenLoaded(user));
    }).catchError((_) {
      emit(CurrentuserscreenFail());
    });
  }

  void logout() {
    _userCenter.logout().then((value) {
      if (value) {
        ExtendedNavigator.root.pop(true);
      }
    });
  }

  void cancel() {
    ExtendedNavigator.root.pop(false);
  }
}
