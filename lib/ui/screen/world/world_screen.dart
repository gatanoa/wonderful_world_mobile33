import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/widget/buttons_bar.dart';
import 'package:wonderful_world/ui/widget/listitem.dart';
import 'package:wonderful_world/ui/widget/listscrollbarview.dart';

import 'cubit/worldscreen_cubit.dart';

class WorldScreen extends StatefulWidget {
  final World world;
  const WorldScreen({Key key, this.world}) : super(key: key);

  @override
  _WorldScreenState createState() => _WorldScreenState();
}

class _WorldScreenState extends State<WorldScreen> with ScopeStateMixin {
  WorldscreenCubit _cubit;
  S _l10n;
  List<Widget> _listItems;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
    _cubit.load(widget.world);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
    var typeDescription;
    if (widget.world.type == WorldTypes.kGOLb2s23MValue) {
      typeDescription = _l10n.cell_type_0;
    } else if (widget.world.type == WorldTypes.kGOLb3s012345678MValue) {
      typeDescription = _l10n.cell_type_1;
    } else if (widget.world.type == WorldTypes.kGOLb1s012345678MValue) {
      typeDescription = _l10n.cell_type_2;
    } else if (widget.world.type == WorldTypes.kGOLb1s012345678NValue) {
      typeDescription = _l10n.cell_type_3;
    } else if (widget.world.type == WorldTypes.kGOLb3678s34678MValue) {
      typeDescription = _l10n.cell_type_4;
    } else {
      typeDescription = _l10n.cell_type_99;
    }

    _listItems = <Widget>[
      ListItem(
          title: _l10n.worldScreen_card_id,
          description: widget.world.id.toString()),
      ListItem(
          title: _l10n.worldScreen_card_type,
          description: typeDescription),
      ListItem(
          title: _l10n.worldScreen_card_title, description: widget.world.name),
      ListItem(
          title: _l10n.worldScreen_card_size,
          description: '${widget.world.width} x ${widget.world.height}'),
      ListItem(
          title: _l10n.worldScreen_card_description,
          description: widget.world.description)
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.world.name),
      ),
      body: BlocBuilder<WorldscreenCubit, WorldscreenState>(
        cubit: _cubit,
        builder: (context, state) {
          return ListScrollBarView(
            padding: Indents.kDefaultBottomIndention,
            children: [
              ..._listItems,
              if (state is WorldscreenLoaded)
                ListItem(
                  title: _l10n.worldScreen_card_owner,
                  description: state.user.username,
                  trailing: Icon(Icons.keyboard_arrow_right),
                  onDescriptionPressed: _cubit.ownerPressedListener,
                ),
            ],
          );
        },
      ),
      bottomNavigationBar: ButtonsBar(
        actionTitle: _l10n.worldScreen_button_open,
        actionPressed: _cubit.openPressedListener,
      ),
    );
  }
}
