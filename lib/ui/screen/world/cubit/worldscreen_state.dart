part of 'worldscreen_cubit.dart';

@immutable
abstract class WorldscreenState {
  final User user;

  WorldscreenState({this.user});
}

class WorldscreenInitial extends WorldscreenState {}

class WorldscreenLoaded extends WorldscreenState {
  WorldscreenLoaded(User user) : super(user: user);
}
