import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/domain/world_center.dart';

part 'createworldscreen_state.dart';

class CreateworldscreenCubit extends Cubit<CreateworldscreenState> {
  final WorldCenter _worldCenter;
  CreateworldscreenCubit(this._worldCenter) : super(CreateworldscreenInitial());

  void create(
      String name, int type, String description, int width, int height) {
    _setAwait();
    _worldCenter
        .create(name, type, description, width, height)
        .then((isCreated) {
      if (isCreated) {
        ExtendedNavigator.root.pop(false);
      } else {
        setDefault();
      }
    });
  }

  void cancel() {
    ExtendedNavigator.root.pop(false);
  }

  void setDefault() {
    emit(CreateworldscreenInitial());
  }

  void setCorrect() {
    emit(CreateworldscreenCorrect());
  }

  void _setAwait() {
    emit(CreateworldscreenAwait());
  }
}
