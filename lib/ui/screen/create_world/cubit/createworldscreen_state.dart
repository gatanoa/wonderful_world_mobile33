part of 'createworldscreen_cubit.dart';

@immutable
abstract class CreateworldscreenState {}

class CreateworldscreenInitial extends CreateworldscreenState {}

class CreateworldscreenCorrect extends CreateworldscreenState {}

class CreateworldscreenAwait extends CreateworldscreenState {}

