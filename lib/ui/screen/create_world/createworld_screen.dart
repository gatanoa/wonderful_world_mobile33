import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/constants/textinput_formatters.dart';
import 'package:wonderful_world/ui/widget/autodropdown_button.dart';
import 'package:wonderful_world/ui/widget/awaiting_button.dart';
import 'package:wonderful_world/ui/widget/buttons_bar.dart';
import 'package:wonderful_world/ui/widget/singlechildscrollbarview.dart';

import 'cubit/createworldscreen_cubit.dart';

class CreateworldScreen extends StatefulWidget {
  @override
  _CreateworldScreenState createState() => _CreateworldScreenState();
}

class _CreateworldScreenState extends State<CreateworldScreen>
    with ScopeStateMixin {
  S _l10n;
  CreateworldscreenCubit _cubit;

  bool _isNameValidate = false;
  bool _isWidthValidate = false;
  bool _isHeightValidate = false;
  bool _isDescriptionValidate = false;

  String _name;
  int _type = 0;
  int _width;
  int _height;
  String _description;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
  }

  @override
  void didChangeDependencies() {
    _l10n = S.of(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _cubit,
      child: Scaffold(
        appBar: AppBar(
          title: Text(_l10n.createWorldScreen_title),
        ),
        bottomNavigationBar: ButtonsBar(
          actionButton:
              AwaitingButton<CreateworldscreenCubit, CreateworldscreenState>(
            title: _l10n.createWorldScreen_button_create,
            onPressed: _createPressedListener,
            isEnabledDelegat: (state) => state is CreateworldscreenCorrect,
            isAwaitDelegat: (state) => state is CreateworldscreenAwait,
          ),
          cancelPressed: _cancelPressedListener,
        ),
        body: SingleChildScrollBarView(
          child: Container(
            margin: Indents.kDefaultBottomIndention,
            child: Column(
              children: [
                Padding(
                  padding: Indents.kCardIndention,
                  child: TextFormField(
                    autovalidateMode: AutovalidateMode.always,
                    validator: _nameValidator,
                    inputFormatters: TextInputFormatters.fBaseName,
                    decoration: InputDecoration(
                      hintText: _l10n.createWorldScreen_input_title,
                      labelText: _l10n.createWorldScreen_input_title,
                    ),
                  ),
                ),
                Padding(
                  padding: Indents.kCardIndention,
                  child: AutodropdownButton(
                    initValue: _type,
                    items: [
                      DropdownMenuItem(
                        value: WorldTypes.kGOLb2s23MValue,
                        child: Text(_l10n.cell_type_0),
                      ),
                      DropdownMenuItem(
                        value: WorldTypes.kGOLb3s012345678MValue,
                        child: Text(_l10n.cell_type_1),
                      ),
                      DropdownMenuItem(
                        value: WorldTypes.kGOLb1s012345678MValue,
                        child: Text(_l10n.cell_type_2),
                      ),
                      DropdownMenuItem(
                        value: WorldTypes.kGOLb1s012345678NValue,
                        child: Text(_l10n.cell_type_3),
                      ),
                      DropdownMenuItem(
                        value: WorldTypes.kGOLb3678s34678MValue,
                        child: Text(_l10n.cell_type_4),
                      ),
                    ],
                    onChanged: _typeChanged,
                  ),
                ),
                Padding(
                  padding: Indents.kCardIndention,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    autovalidateMode: AutovalidateMode.always,
                    validator: _widthValidator,
                    inputFormatters: TextInputFormatters.fWholeNumber,
                    decoration: InputDecoration(
                      hintText: '0',
                      labelText: _l10n.createWorldScreen_input_width,
                    ),
                  ),
                ),
                Padding(
                  padding: Indents.kCardIndention,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    autovalidateMode: AutovalidateMode.always,
                    validator: _heightValidator,
                    inputFormatters: TextInputFormatters.fWholeNumber,
                    decoration: InputDecoration(
                      hintText: '0',
                      labelText: _l10n.createWorldScreen_input_height,
                    ),
                  ),
                ),
                Padding(
                  padding: Indents.kCardIndention,
                  child: TextFormField(
                    autovalidateMode: AutovalidateMode.always,
                    validator: _descriptionValidator,
                    inputFormatters: TextInputFormatters.fText,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: _l10n.createWorldScreen_input_description,
                      labelText: _l10n.createWorldScreen_input_description,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _nameValidator(String value) {
    var result;
    var stepValidate = value != null && value.isNotEmpty;
    if (!stepValidate) {
      result = _l10n.createWorldScreen_input_title_validate_fail;
    }
    _name = value;
    if (_isNameValidate != stepValidate) {
      _isNameValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  void _typeChanged(int value) {
    _type = value;
  }

  String _widthValidator(String value) {
    var result;
    var stepValidate = value != null && value.isNotEmpty;
    if (!stepValidate) {
      result = _l10n.createWorldScreen_input_width_validate_fail_0;
    } else {
      try {
        _width = int.parse(value);
        if (_width == 0) {
          result = _l10n.createWorldScreen_input_width_validate_fail_1;
          stepValidate = false;
        }
      } catch (ex) {
        result = ex.toString();
      }
    }

    if (_isWidthValidate != stepValidate) {
      _isWidthValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  String _heightValidator(String value) {
    var result;
    var stepValidate = value != null && value.isNotEmpty;
    if (!stepValidate) {
      result = _l10n.createWorldScreen_input_height_validate_fail_0;
    } else {
      try {
        _height = int.parse(value);
        if (_height == 0) {
          result = _l10n.createWorldScreen_input_height_validate_fail_1;
          stepValidate = false;
        }
      } catch (ex) {
        result = ex.toString();
      }
    }

    if (_isHeightValidate != stepValidate) {
      _isHeightValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  String _descriptionValidator(String value) {
    var result;
    var stepValidate = value != null && value.isNotEmpty;
    if (!stepValidate) {
      result = _l10n.createWorldScreen_input_description_validate_fail;
    }
    _description = value;
    if (_isDescriptionValidate != stepValidate) {
      _isDescriptionValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  bool get isAllValidate =>
      _isNameValidate &&
      _isWidthValidate &&
      _isHeightValidate &&
      _isDescriptionValidate;

  Future<void> _changedListener() async {
    if (isAllValidate) {
      _cubit.setCorrect();
      return;
    }
    if (_cubit.state is CreateworldscreenCorrect) {
      _cubit.setDefault();
    }
  }

  void _createPressedListener() {
    _cubit.create(_name, _type, _description, _width, _height);
  }

  void _cancelPressedListener() {
    _cubit.cancel();
  }
}
