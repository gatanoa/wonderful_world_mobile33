import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'createworld_screen.dart';
import 'cubit/createworldscreen_cubit.dart';

var createworldModule = Module()
  ..scopeOneCubit<CreateworldscreenCubit, CreateworldScreen>(
    (scope) => CreateworldscreenCubit(scope.get()),
  );
