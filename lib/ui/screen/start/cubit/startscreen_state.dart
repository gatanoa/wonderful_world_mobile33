part of 'startscreen_cubit.dart';

@immutable
abstract class StartscreenState {
  final bool isLastWorldExists;
  final World lastWorld;

  StartscreenState({this.isLastWorldExists = false, this.lastWorld});
}

class StartscreenAwait extends StartscreenState {}

class StartscreenFail extends StartscreenState {
  StartscreenFail(World lastWorld)
      : super(
          isLastWorldExists: lastWorld.type != null,
          lastWorld: lastWorld,
        );
}
