import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'package:wonderful_world/ui/screen/start/cubit/startscreen_cubit.dart';
import 'package:wonderful_world/ui/screen/start/start_screen.dart';

var startscreenModule = Module()
  ..scopeOneCubit<StartscreenCubit, StartScreen>(
    (scope) => StartscreenCubit(scope.get(), scope.get(), scope.get()),
  );
