import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:wonderful_world/domain/settings_center.dart';
import 'package:wonderful_world/domain/user_center.dart';
import 'package:wonderful_world/domain/warp_reflection/warpreflection_center.dart';
import 'package:wonderful_world/domain/world_center.dart';
import 'package:wonderful_world/ui/widget/colorpicker_dialog.dart';

import '../constants.dart';
import '../reflection_screen.dart';

part 'reflectionscreen_state.dart';

class ReflectionscreenCubit extends Cubit<ReflectionscreenState> {
  final SettingsCenter _settingsCenter;
  final WarpReflectionCenter _warpCenter;
  final UserCenter _userCenter;
  final WorldCenter _worldCenter;
  StreamSubscription _changedSubscription;
  StreamSubscription _stoppedSubscription;
  StreamSubscription _authSubscription;

  String _ownerId;

  ReflectionscreenCubit(
    this._settingsCenter,
    this._warpCenter,
    this._userCenter,
    this._worldCenter,
  ) : super(ReflectionscreenInitial()) {
    _changedSubscription = _warpCenter.changedObservable.listen(
      (_) => _onChangedListener(),
    );
    _stoppedSubscription = _warpCenter.stoppedObservable.listen(
      (_) => _onStoppedListener(),
    );
    _authSubscription = _userCenter.authStateChangeObservable.listen(
      _onAuthChanged,
    );
    _userCenter.isAuth().then(_onAuthChanged).catchError((_) {
      _onAuthChanged(false);
    });
  }

  void _onAuthChanged(bool isAuth) {
    _stateChange((state) {
      return state..isAuth = isAuth;
    });
    _onOwnerIdChanged(_ownerId);
  }

  void _onOwnerIdChanged(String ownerId) {
    if (state.isAuth) {
      _userCenter.current().then((user) {
        _stateChange((state) {
          return state..isOwner = ownerId == user.id;
        });
      });
    } else {
      _stateChange((state) {
        return state..isOwner = false;
      });
    }
  }

  void _onChangedListener() {
    _stateChange((state) {
      return state..isEdited = true;
    });
  }

  void _onStoppedListener() {
    _stateChange((state) {
      return state..isPlayed = false;
    });
  }

  Future<void> _loadColors() async {
    var bufferColors = PanelItemColors(
      background: await _settingsCenter.getReflectionBackgroundColor(),
      greed: await _settingsCenter.getReflectionGreedColor(),
      cells: <Color>[
        await _settingsCenter.getReflectionCell0Color(),
      ],
    );
    _stateChange((state) {
      state.elementColors = bufferColors;
      return state;
    });
  }

  void lastDataLoad() {
    _loadColors().then((_) {
      _warpCenter.startIfStopped().then((value) {
        _warpCenter.lastDataLoading().then((isLoaded) {
          if (isLoaded) {
            _warpCenter.jiffMillisecDuration().then((millisec) {
              state.jiffMillisecDuration = millisec.toDouble();
              emit(ReflectionscreenBrowsing(source: state));
            });
          } else {
            emit(ReflectionscreenLoadingFail(source: state));
          }
        });
      });
    });
  }

  void load(String id, String ownerId) {
    _ownerId = ownerId;
    _onOwnerIdChanged(ownerId);

    _loadColors().then((_) {
      _warpCenter.startIfStopped().then((value) {
        _warpCenter.loading(id).then((isLoaded) {
          if (isLoaded) {
            _warpCenter.jiffMillisecDuration().then((millisec) async {
              state.jiffMillisecDuration = millisec.toDouble();
              await _worldCenter.setLastWorld(id);
              emit(ReflectionscreenBrowsing(source: state));
            });
          } else {
            emit(ReflectionscreenLoadingFail(source: state));
          }
        });
      });
    });
  }

  void setJiffMillisecDurationListener(double millisec) {
    double bufferMillisec = millisec.roundToDouble();
    if (bufferMillisec != state.jiffMillisecDuration) {
      _stateChange((state) {
        return state..jiffMillisecDuration = bufferMillisec;
      });
      _warpCenter.setJiffMilliSecDuration(bufferMillisec.round());
    }
  }

  void save() {
    _warpCenter.save().then((isSaved) {
      _stateChange((state) {
        return state..isEdited = !isSaved;
      });
    });
  }

  void cancelPressedListener() {
    ExtendedNavigator.root.pop();
  }

  void swapPressedListener() {
    emit(state.swap());
  }

  void bottombarPressedListener() {
    _stateChange((state) {
      return state..isBottombarOpen = !state.isBottombarOpen;
    });
  }

  void playpausePressedListener() {
    if (state.isPlayed) {
      _warpCenter.stop();
      _stateChange((state) {
        return state..isPlayed = false;
      });
    } else {
      _warpCenter.start();
      _stateChange((state) {
        return state..isPlayed = true;
      });
    }
  }

  void backgroundcolorPressedListener(BuildContext context) {
    ColorpickerDialog.show(context, state.elementColors.background)
        .then((color) async {
      if (color != state.elementColors.background) {
        await _settingsCenter.setReflectionBackgroundColor(color);
        _stateChange((state) {
          return state..elementColors.background = color;
        });
      }
    });
  }

  void greedcolorPressedListener(BuildContext context) {
    ColorpickerDialog.show(context, state.elementColors.greed)
        .then((color) async {
      if (color != state.elementColors.greed) {
        await _settingsCenter.setReflectionGreedColor(color);
        _stateChange((state) {
          return state..elementColors.greed = color;
        });
      }
    });
  }

  void itemcolorPressedListener(BuildContext context, int index) {
    ColorpickerDialog.show(context, state.elementColors.cells[index])
        .then((color) async {
      if (color != state.elementColors.cells[index]) {
        if (index == 0) {
          await _settingsCenter.setReflectionCell0Color(color);
        }
        _stateChange((state) {
          return state..elementColors.cells[index] = color;
        });
      }
    });
  }

  void resetcolorPressedListener() {
    _settingsCenter.resetReflectionColors().then((_) {
      _loadColors();
    });
  }

  void editTypeChangeListener(int type) {
    if (state.editType != type) {
      _stateChange((state) {
        return state..editType = type;
      });
    }
  }

  void _stateChange(
      ReflectionscreenState Function(ReflectionscreenState state) callback) {
    emit(callback(state.copy()));
  }

  @override
  Future<void> close() async {
    _authSubscription.cancel();
    _changedSubscription.cancel();
    _stoppedSubscription.cancel();
    _warpCenter.clear();
    await super.close();
  }
}
