part of 'reflectionscreen_cubit.dart';

abstract class ReflectionscreenState {
  bool isEdited;
  int editType;
  bool isAuth;
  bool isOwner;
  bool isBottombarOpen;
  bool isPlayed;
  double jiffMillisecDuration;
  PanelItemColors elementColors;

  ReflectionscreenState({
    ReflectionscreenState source,
    bool isEdited,
    int editType,
    bool isAuth,
    bool isOwner,
    bool isBottombarOpen,
    bool isPlayed,
    double jiffMillisecDuration,
    PanelItemColors elementColors,
  })  : this.isEdited = isEdited ?? source?.isEdited ?? false,
        this.editType = editType ?? source?.editType ?? 0,
        this.isAuth = isAuth ?? source?.isAuth ?? false,
        this.isOwner = isOwner ?? source?.isOwner ?? false,
        this.isBottombarOpen =
            isBottombarOpen ?? source?.isBottombarOpen ?? false,
        this.isPlayed = isPlayed ?? source?.isPlayed ?? false,
        this.jiffMillisecDuration =
            jiffMillisecDuration ?? source?.jiffMillisecDuration ?? 0,
        this.elementColors =
            elementColors ?? source?.elementColors ?? fDefaultElementColors;

  ReflectionscreenState copy();

  ReflectionscreenState swap();
}

class ReflectionscreenBrowsing extends ReflectionscreenState {
  ReflectionscreenBrowsing({ReflectionscreenState source})
      : super(source: source);

  @override
  ReflectionscreenBrowsing copy() {
    return ReflectionscreenBrowsing(source: this);
  }

  @override
  ReflectionscreenEditing swap() {
    return ReflectionscreenEditing(source: this);
  }
}

class ReflectionscreenEditing extends ReflectionscreenState {
  ReflectionscreenEditing({ReflectionscreenState source})
      : super(source: source);

  @override
  ReflectionscreenEditing copy() {
    return ReflectionscreenEditing(source: this);
  }

  @override
  ReflectionscreenBrowsing swap() {
    return ReflectionscreenBrowsing(source: this);
  }
}

class ReflectionscreenInitial extends ReflectionscreenState {
  ReflectionscreenInitial({ReflectionscreenState source})
      : super(source: source);

  @override
  ReflectionscreenInitial copy() {
    return ReflectionscreenInitial(source: this);
  }

  @override
  ReflectionscreenInitial swap() {
    return this;
  }
}

class ReflectionscreenLoadingFail extends ReflectionscreenState {
  ReflectionscreenLoadingFail({ReflectionscreenState source})
      : super(source: source);

  @override
  ReflectionscreenLoadingFail copy() {
    return ReflectionscreenLoadingFail(source: this);
  }

  @override
  ReflectionscreenLoadingFail swap() {
    return this;
  }
}
