import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';

import 'component/body/di_module.dart';
import 'cubit/reflectionscreen_cubit.dart';
import 'reflection_screen.dart';

var reflectionModules = [
  reflectionbodycomponentModule,
  Module()
    ..scopeOneCubit<ReflectionscreenCubit, ReflectionScreen>(
      (scope) => ReflectionscreenCubit(
        scope.get(),
        scope.get(),
        scope.get(),
        scope.get(),
      ),
    ),
];
