import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wonderful_world/generated/l10n.dart';

import '../cubit/reflectionscreen_cubit.dart';

class AppBarComponent extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReflectionscreenCubit, ReflectionscreenState>(
      buildWhen: (previous, current) =>
          previous.runtimeType != current.runtimeType ||
          previous.isAuth != current.isAuth ||
          previous.isEdited != current.isEdited,
      builder: (context, state) {
        var l10n = S.of(context);
        var cubit = BlocProvider.of<ReflectionscreenCubit>(context);
        return AppBar(
          title: Text(
            (state is ReflectionscreenBrowsing)
                ? l10n.reflectionScreen_title_browsing
                : l10n.reflectionScreen_title_editing,
          ),
          actions: (state.isAuth && state.isOwner)
              ? [
                  IconButton(
                      icon: Icon(Icons.save),
                      onPressed: (state.isEdited) ? cubit.save : null)
                ]
              : null,
        );
      },
    );
  }
}
