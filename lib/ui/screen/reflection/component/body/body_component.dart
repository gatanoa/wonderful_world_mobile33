import 'package:flutter/material.dart';

import '../../reflection_screen.dart';
import 'gol_component.dart';

abstract class BodyComponent extends StatefulWidget {
  final Size size;
  final int worldWidth;
  final int worldHeight;
  final double gridWidth;
  final double cellWidth;
  final PanelItemColors elementColors;

  const BodyComponent(
    this.size,
    this.worldWidth,
    this.worldHeight,
    this.gridWidth,
    this.cellWidth,
    this.elementColors,
  );

  factory BodyComponent.fromWorldType(
    int type,
    Size size,
    int worldWidth,
    int worldHeight,
    PanelItemColors elementColors,
    double gridWidth,
    double cellWidth,
  ) {
    // WorldTypes.kGOLValue
    return GOLComponent(
      size,
      worldWidth,
      worldHeight,
      gridWidth,
      cellWidth,
      elementColors,
    );
  }
}
