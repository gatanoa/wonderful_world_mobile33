part of 'reflectionbodycomponent_cubit.dart';

@immutable
abstract class ReflectionbodycomponentState {
  final List<ReflectItem> items;

  ReflectionbodycomponentState({this.items});
}

class ReflectionbodycomponentInitial extends ReflectionbodycomponentState {
  ReflectionbodycomponentInitial() : super(items: const <ReflectItem>[]);
}

class ReflectionbodycomponentShow extends ReflectionbodycomponentState {
  ReflectionbodycomponentShow(List<ReflectItem> items) : super(items: items);
}
