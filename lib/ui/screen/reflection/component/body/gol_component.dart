import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/domain/warp_reflection/model/gol_reflectitem.dart.dart';
import 'package:wonderful_world/domain/warp_reflection/model/reflect_item.dart';
import 'package:wonderful_world/ui/screen/reflection/component/body/cubit/reflectionbodycomponent_cubit.dart';
import 'package:wonderful_world/ui/screen/reflection/cubit/reflectionscreen_cubit.dart';

import '../../reflection_screen.dart';
import 'body_component.dart';

class GOLComponent extends BodyComponent {
  GOLComponent(
    Size size,
    int worldWidth,
    int worldHeight,
    double gridWidth,
    double cellWidth,
    PanelItemColors elementColors,
  )   : assert(elementColors != null),
        super(
          size,
          worldWidth,
          worldHeight,
          gridWidth,
          cellWidth,
          elementColors,
        );

  @override
  State<GOLComponent> createState() => _GOLComponentState();

  static List<String> getPanelItemTitles(BuildContext context) {
    var l10n = S.of(context);
    return <String>[
      l10n.reflectionScreen_panel_browsing_item_color_0_gof,
    ];
  }
}

class _GOLComponentState extends State<GOLComponent> with ScopeStateMixin {
  ReflectionbodycomponentCubit _cubit;
  List<GOLReflectItem> _cells;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
    _setCells(_cubit.state.items);
  }

  void _setCells(List<ReflectItem> items) {
    _cells = items.map((e) => e as GOLReflectItem).toList();
  }

  void _onTap(TapUpDetails info, int editType) {
    var bufferElementWidth = widget.cellWidth + widget.gridWidth;
    var dx = info.localPosition.dx - widget.gridWidth;
    var dy = info.localPosition.dy - widget.gridWidth;
    var point =
        Point<int>(dx ~/ bufferElementWidth, dy ~/ bufferElementWidth);
    if (editType > 0) {
      _cubit.insert(point, editType);
    } else {
      _cubit.remove(point);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReflectionscreenCubit, ReflectionscreenState>(
      builder: (context, state) {
        return GestureDetector(
          onTapUp: (state is ReflectionscreenEditing)
              ? (info) => _onTap(info, state.editType)
              : null,
          child: CustomPaint(
            size: widget.size,
            painter: _BackgroundPainter(
              worldWidth: widget.worldWidth,
              worldHeight: widget.worldHeight,
              gridWidth: widget.gridWidth,
              cellWidth: widget.cellWidth,
              backgroundColor: widget.elementColors.background,
              gridColor: widget.elementColors.greed,
            ),
            child: BlocListener<ReflectionbodycomponentCubit,
                ReflectionbodycomponentState>(
              cubit: _cubit,
              listenWhen: (previous, current) =>
                  previous.items != current.items,
              listener: (_, state) {
                _setCells(state.items);
              },
              child: BlocBuilder<ReflectionbodycomponentCubit,
                  ReflectionbodycomponentState>(
                cubit: _cubit,
                builder: (_, __) {
                  return _CellsPaint(
                    size: widget.size,
                    gridWidth: widget.gridWidth,
                    cellWidth: widget.cellWidth,
                    cellColor: widget.elementColors.cells[0],
                    cells: _cells,
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }
}

class _BackgroundPainter extends CustomPainter {
  final Paint _paint;
  final double _halfGridWidth;
  final double _cellWidth;
  final int worldWidth;
  final int worldHeight;
  final Color backgroundColor;

  _BackgroundPainter({
    @required this.worldWidth,
    @required this.worldHeight,
    @required double gridWidth,
    @required double cellWidth,
    @required this.backgroundColor,
    @required Color gridColor,
  })  : _paint = Paint()
          ..color = gridColor
          ..strokeWidth = gridWidth,
        _halfGridWidth = gridWidth / 2,
        _cellWidth = cellWidth + gridWidth;

  @override
  void paint(Canvas canvas, Size _size) {
    if (backgroundColor != null) {
      canvas.drawRect(
        Rect.fromLTWH(0.0, 0.0, _size.width, _size.height),
        Paint()..color = backgroundColor,
      );
    }
    for (int i = 0; i <= worldWidth; ++i) {
      var x = _halfGridWidth + i * _cellWidth;
      canvas.drawLine(
        Offset(x, 0.0),
        Offset(x, _size.height),
        _paint,
      );
    }
    for (int i = 0; i <= worldHeight; ++i) {
      var y = _halfGridWidth + i * _cellWidth;
      canvas.drawLine(
        Offset(0.0, y),
        Offset(_size.width, y),
        _paint,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class _CellsPaint extends StatefulWidget {
  final Size size;
  final double gridWidth;
  final double cellWidth;
  final Color cellColor;
  final List<GOLReflectItem> cells;

  const _CellsPaint({
    Key key,
    @required this.size,
    @required this.gridWidth,
    @required this.cellWidth,
    @required this.cellColor,
    @required this.cells,
  }) : super(key: key);

  @override
  _CellsPaintState createState() => _CellsPaintState();
}

class _CellsPaintState extends State<_CellsPaint> {
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      size: widget.size,
      painter: _CellsPainter(
        gridWidth: widget.gridWidth,
        cellWidth: widget.cellWidth,
        cellColor: widget.cellColor,
        cells: widget.cells,
      ),
    );
  }
}

class _CellsPainter extends CustomPainter {
  final double _biasItem;
  final Paint _paint;
  final double gridWidth;
  final double cellWidth;
  final List<GOLReflectItem> cells;

  _CellsPainter({
    @required this.gridWidth,
    @required this.cellWidth,
    @required Color cellColor,
    @required this.cells,
  })  : _biasItem = cellWidth + gridWidth,
        _paint = Paint()..color = cellColor;

  @override
  void paint(Canvas canvas, Size size) {
    for (var iter in cells) {
      var left = iter.x * _biasItem + gridWidth;
      var top = iter.y * _biasItem + gridWidth;
      canvas.drawRect(
        Rect.fromLTWH(
          left,
          top,
          cellWidth,
          cellWidth,
        ),
        _paint,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
