import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:wonderful_world/ui/widget/zoom/zoom.dart';

import '../cubit/reflectionscreen_cubit.dart';
import '../constants.dart';
import 'body/body_component.dart';

class ZoomComponent extends StatefulWidget {
  final EdgeInsets margin;
  final int type;
  final int worldWidth;
  final int worldHeight;
  final double gridWidth;
  final double cellWidth;

  const ZoomComponent({
    Key key,
    this.margin,
    @required this.type,
    @required this.worldWidth,
    @required this.worldHeight,
    this.gridWidth = kDefaultGridWidth,
    this.cellWidth = kDefaultCellWidth,
  }) : super(key: key);

  @override
  _ZoomComponentState createState() => _ZoomComponentState();
}

class _ZoomComponentState extends State<ZoomComponent> {
  bool isInitialized;
  Size _size;
  double _gridWidth;
  double _cellWidth;

  void _sizeUpdate() {
    var width = widget.worldWidth * (_cellWidth + _gridWidth) + _gridWidth;
    var height = widget.worldHeight * (_cellWidth + _gridWidth) + _gridWidth;
    _size = Size(width, height);
  }

  @override
  void initState() {
    super.initState();
    isInitialized = false;
    _gridWidth = widget.gridWidth;
    _cellWidth = widget.cellWidth;
    _sizeUpdate();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReflectionscreenCubit, ReflectionscreenState>(
      builder: (context, state) {
        return Container(
          margin: widget.margin,
          alignment: Alignment.center,
          child: Zoom(
            enableShadow: false,
            initZoom: 0.0,
            backgroundColor: Colors.transparent,
            canvasColor: Colors.transparent,
            width: _size.width,
            height: _size.height,
            doubleTapZoom: state is ReflectionscreenBrowsing,
            child: BodyComponent.fromWorldType(
              widget.type,
              _size,
              widget.worldWidth,
              widget.worldHeight,
              state.elementColors,
              _gridWidth,
              _cellWidth,
            ),
          ),
        );
      },
    );
  }
}
