part of 'theme_cubit.dart';

@immutable
abstract class ThemeState {
  final bool isDark;

  ThemeState(this.isDark);
}

class ThemeChange extends ThemeState {
  ThemeChange(bool isDark) : super(isDark);
}
