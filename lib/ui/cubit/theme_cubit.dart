import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:flutter/material.dart';
import 'package:wonderful_world/domain/settings_center.dart';

part 'theme_state.dart';

class ThemeCubit extends Cubit<ThemeState> {
  final SettingsCenter _settingsCenter;
  StreamSubscription<bool> _darkModeStateSubscription;
  ThemeCubit(this._settingsCenter) : super(ThemeChange(true)) {
    _darkModeStateSubscription =
        _settingsCenter.darkModeStateObservable.listen(_themeChangeListener);
  }

  Future<void> update() async {
    var status = false;
    try {
      status = await _settingsCenter.getDarkModeState();
    } catch (_) {}
    emit(ThemeChange(status));
  }

  Future<void> toSwitch() async {
    var isDark = !this.state.isDark;
    _settingsCenter.setDarkModeState(isDark).then((value) {
      emit(ThemeChange(isDark));
    });
  }

  void _themeChangeListener(bool state) {
    emit(ThemeChange(state));
  }

  @override
  Future<void> close() async {
    await _darkModeStateSubscription.cancel();
    await super.close();
  }
}
