import 'package:flutter/material.dart';

class Indents {
  static const kCardIndention =
      const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0);
  static const kDefaultHorizontalIndention =
      const EdgeInsets.symmetric(horizontal: 10.0);
  static const kDefaultVerticalIndention =
      const EdgeInsets.symmetric(vertical: 10.0);
  static const kDefaultBottomIndention = const EdgeInsets.only(bottom: 10.0);
  static const kDefaultLeftIndention = const EdgeInsets.only(left: 10.0);
  static const kDefaultAllIndention = const EdgeInsets.all(10.0);
}
