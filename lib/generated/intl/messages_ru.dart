// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  static m0(count) => "Создано миров: ${count}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "button_cancel" : MessageLookupByLibrary.simpleMessage("Отмена"),
    "button_ok" : MessageLookupByLibrary.simpleMessage("Ок"),
    "cell_type_0" : MessageLookupByLibrary.simpleMessage("Игра \"Жизнь\" (B2/S23 - M)"),
    "cell_type_1" : MessageLookupByLibrary.simpleMessage("Игра \"Жизнь\" (B3/S012345678 - M)"),
    "cell_type_2" : MessageLookupByLibrary.simpleMessage("Игра \"Жизнь\" (B1/S012345678 - M)"),
    "cell_type_3" : MessageLookupByLibrary.simpleMessage("Игра \"Жизнь\" (B1/S012345678 - N)"),
    "cell_type_4" : MessageLookupByLibrary.simpleMessage("День и ночь (B3678/S34678 - M)"),
    "cell_type_99" : MessageLookupByLibrary.simpleMessage("Новый тип"),
    "contextMenu_item_clone" : MessageLookupByLibrary.simpleMessage("Клонировать"),
    "contextMenu_item_remove" : MessageLookupByLibrary.simpleMessage("Удалить"),
    "contextMenu_item_selected_add" : MessageLookupByLibrary.simpleMessage("Добавить в избранное"),
    "contextMenu_item_selected_remove" : MessageLookupByLibrary.simpleMessage("Удалить из избранных"),
    "createWorldScreen_button_create" : MessageLookupByLibrary.simpleMessage("Создать"),
    "createWorldScreen_input_description" : MessageLookupByLibrary.simpleMessage("Описание"),
    "createWorldScreen_input_description_validate_fail" : MessageLookupByLibrary.simpleMessage("Пожалуйста, введите  описание"),
    "createWorldScreen_input_height" : MessageLookupByLibrary.simpleMessage("Высота"),
    "createWorldScreen_input_height_validate_fail_0" : MessageLookupByLibrary.simpleMessage("Пожалуйста, введите  высоту"),
    "createWorldScreen_input_height_validate_fail_1" : MessageLookupByLibrary.simpleMessage(" > 0"),
    "createWorldScreen_input_title" : MessageLookupByLibrary.simpleMessage("Название"),
    "createWorldScreen_input_title_validate_fail" : MessageLookupByLibrary.simpleMessage("Пожалуйста, введите  название"),
    "createWorldScreen_input_type" : MessageLookupByLibrary.simpleMessage("Тип"),
    "createWorldScreen_input_width" : MessageLookupByLibrary.simpleMessage("Ширина"),
    "createWorldScreen_input_width_validate_fail_0" : MessageLookupByLibrary.simpleMessage("Пожалуйста, введите  ширину"),
    "createWorldScreen_input_width_validate_fail_1" : MessageLookupByLibrary.simpleMessage(" > 0"),
    "createWorldScreen_title" : MessageLookupByLibrary.simpleMessage("Создание мира"),
    "currentUserScreen_button_logout" : MessageLookupByLibrary.simpleMessage("Выйти"),
    "currentUserScreen_title" : MessageLookupByLibrary.simpleMessage("Текущий пользователь"),
    "homeScreen_drawer_darkTheme" : MessageLookupByLibrary.simpleMessage("Тёмная тема"),
    "homeScreen_drawer_userCard_button_auth" : MessageLookupByLibrary.simpleMessage("Перейти в профиль"),
    "homeScreen_drawer_userCard_button_unauth" : MessageLookupByLibrary.simpleMessage("Войти"),
    "homeScreen_drawer_userCard_title_default" : MessageLookupByLibrary.simpleMessage("Пользователь отсутствует"),
    "homeScreen_title_available" : MessageLookupByLibrary.simpleMessage("Доступные миры"),
    "homeScreen_title_created" : MessageLookupByLibrary.simpleMessage("Созданные миры"),
    "homeScreen_title_selected" : MessageLookupByLibrary.simpleMessage("Избранные миры"),
    "loginScreen_button_logIn" : MessageLookupByLibrary.simpleMessage("Войти"),
    "loginScreen_button_signUp" : MessageLookupByLibrary.simpleMessage("Зарегистрироваться"),
    "loginScreen_input_email" : MessageLookupByLibrary.simpleMessage("Електронная почта"),
    "loginScreen_input_email_exists" : MessageLookupByLibrary.simpleMessage("Электронная почта занята"),
    "loginScreen_input_email_validate_fail" : MessageLookupByLibrary.simpleMessage("Пожалуйста, введите адрес электронной почты"),
    "loginScreen_input_password" : MessageLookupByLibrary.simpleMessage("Пароль"),
    "loginScreen_input_password_validate_fail" : MessageLookupByLibrary.simpleMessage("Пожалуйста, введите пароль"),
    "loginScreen_input_repeatPassword" : MessageLookupByLibrary.simpleMessage("Подтвердите пароль"),
    "loginScreen_input_repeatPassword_validate_fail" : MessageLookupByLibrary.simpleMessage("Пожалуйста, подтвердите пароль"),
    "loginScreen_input_username" : MessageLookupByLibrary.simpleMessage("Имя пользователя"),
    "loginScreen_input_username_exists" : MessageLookupByLibrary.simpleMessage("Имя пользователя занято"),
    "loginScreen_input_username_validate_fail" : MessageLookupByLibrary.simpleMessage("Пожалуйста, введите имя пользователя"),
    "loginScreen_link_logIn" : MessageLookupByLibrary.simpleMessage("Уже зарегистрированы? Войти здесь"),
    "loginScreen_link_signUp" : MessageLookupByLibrary.simpleMessage("Не зарегистрированы? Зарегистрируйтесь здесь"),
    "loginScreen_title_logIn" : MessageLookupByLibrary.simpleMessage("Вход"),
    "loginScreen_title_signUp" : MessageLookupByLibrary.simpleMessage("Регистрация"),
    "notification_content_add" : MessageLookupByLibrary.simpleMessage("Добавлено"),
    "notification_content_clone" : MessageLookupByLibrary.simpleMessage("Скопировано"),
    "notification_content_create" : MessageLookupByLibrary.simpleMessage("Создано"),
    "notification_content_delete" : MessageLookupByLibrary.simpleMessage("Удалено!"),
    "reflectionScreen_button_options" : MessageLookupByLibrary.simpleMessage("Опции"),
    "reflectionScreen_button_play" : MessageLookupByLibrary.simpleMessage("Пуск"),
    "reflectionScreen_button_stop" : MessageLookupByLibrary.simpleMessage("Стоп"),
    "reflectionScreen_card_loadingFail" : MessageLookupByLibrary.simpleMessage("Сбой загрузки"),
    "reflectionScreen_panel_browsing_item_background" : MessageLookupByLibrary.simpleMessage("Фон"),
    "reflectionScreen_panel_browsing_item_color_0_gof" : MessageLookupByLibrary.simpleMessage("Клетка"),
    "reflectionScreen_panel_browsing_item_grid" : MessageLookupByLibrary.simpleMessage("Сетка"),
    "reflectionScreen_panel_browsing_item_reset" : MessageLookupByLibrary.simpleMessage("Сброс"),
    "reflectionScreen_panel_editing_item_pain_0_gof" : MessageLookupByLibrary.simpleMessage("Клетка"),
    "reflectionScreen_panel_editing_item_remove" : MessageLookupByLibrary.simpleMessage("Стереть"),
    "reflectionScreen_title_browsing" : MessageLookupByLibrary.simpleMessage("Просмотр"),
    "reflectionScreen_title_editing" : MessageLookupByLibrary.simpleMessage("Редактирование"),
    "reflectionScreen_title_loading" : MessageLookupByLibrary.simpleMessage("Загрузка"),
    "searchWorldsScreen_button_search" : MessageLookupByLibrary.simpleMessage("Поиск"),
    "searchWorldsScreen_label_authorId" : MessageLookupByLibrary.simpleMessage("Id Автора"),
    "searchWorldsScreen_label_authorName" : MessageLookupByLibrary.simpleMessage("Автор"),
    "searchWorldsScreen_label_name" : MessageLookupByLibrary.simpleMessage("Название"),
    "searchWorldsScreen_title" : MessageLookupByLibrary.simpleMessage("Поиск"),
    "startScreen_button_closeApp" : MessageLookupByLibrary.simpleMessage("Закрыть приложение"),
    "startScreen_button_lastWorldOpen" : MessageLookupByLibrary.simpleMessage("Мир открытый последним"),
    "startScreen_button_tryAgain" : MessageLookupByLibrary.simpleMessage("Попробовать снова"),
    "startScreen_text_failMessage" : MessageLookupByLibrary.simpleMessage("Ошибка подключения"),
    "userWidget_card_email" : MessageLookupByLibrary.simpleMessage("Электронная почта:"),
    "userWidget_card_id" : MessageLookupByLibrary.simpleMessage("Id:"),
    "userWidget_card_username" : MessageLookupByLibrary.simpleMessage("Имя:"),
    "userWidget_card_worldsCreated" : m0,
    "userWidget_card_worldsCreated_button" : MessageLookupByLibrary.simpleMessage("Показать"),
    "worldScreen_button_open" : MessageLookupByLibrary.simpleMessage("Открыть"),
    "worldScreen_card_description" : MessageLookupByLibrary.simpleMessage("Описание:"),
    "worldScreen_card_id" : MessageLookupByLibrary.simpleMessage("Id:"),
    "worldScreen_card_owner" : MessageLookupByLibrary.simpleMessage("Владелец:"),
    "worldScreen_card_size" : MessageLookupByLibrary.simpleMessage("Рзамеры:"),
    "worldScreen_card_title" : MessageLookupByLibrary.simpleMessage("Название:"),
    "worldScreen_card_type" : MessageLookupByLibrary.simpleMessage("Тип")
  };
}
